<?php

/**
 * @package           Levup
 * @author            Rehan Khan
 * @license           GPL-2.0-or-later
 * Plugin Name:       Assignment/ task feature for users in a membership WP website
 * Plugin URI:        https://gitlab.com/vincent-king/assignment-task-feature-for-users-in-a-membership-wp-website
 * Description:       Adds Assignment Features to a BuddyBoss site.
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Rehan Khan
 * Author URI:        https://www.codeable.io/developers/rehan-khan/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       levup
 * Domain Path:       /languages
 */

if ( ! function_exists( 'add_action' ) ) {
	esc_html_e( 'Hi there!  I\'m just a plugin, not much I can do when called directly.', 'codeable' );
	exit;
}

/**
 *  Check if BuddyBoss and Gamipress are active
 */

$active_plugins = get_option( 'active_plugins' );

$buddy_boss_exists = in_array( 'buddyboss-platform/bp-loader.php', $active_plugins, true );
$gamipress_exists  = in_array( 'gamipress/gamipress.php', $active_plugins, true );
$buddy_boss_pro_exists = in_array( 'buddyboss-platform-pro/buddyboss-platform-pro.php', $active_plugins, true );

$buddy_boss_theme_exists = wp_get_theme()->name == 'BuddyBoss Theme' ? true : false;

// Includes
require 'utilities/gamipress-buddyboss-not-activated.php';
require 'includes/activation.php';
require 'utilities/levup-add-profile-fields.php';
require 'utilities/create-default-pages.php';

// Hook
register_activation_hook( __FILE__, 'levup_plugin_activate' );

if ( $buddy_boss_exists && $gamipress_exists && $buddy_boss_pro_exists && $buddy_boss_theme_exists ) {

	add_action(
		'plugins_loaded',
		function() {
			// Setup.
			define( 'LEVUP_PATH', __FILE__ );
			define( 'LEVUP_DIR_PATH', plugin_dir_path( __FILE__ ) );

			// Includes.
			require 'includes/enqueue.php';
			require 'utilities/register-shortcodes.php';
			require 'utilities/get-xprofile-data.php';
			require 'utilities/check-mentorship-availability.php';
			require 'utilities/get-current-mentorships.php';
			require 'process/levup-filter-mentors.php';
			require 'process/levup-request-mentorship.php';
			require 'process/levup-add-mentorship-task.php';
			require 'process/levup-task-process.php';
			require 'utilities/get-mentorship-history.php';
			require 'process/levup-auto-match.php';
			require 'process/levup-update-mentorship-status.php';
			require 'process/levup-upload-mentorship-file.php';
			require 'utilities/send-email-notification.php';
			require 'includes/gamipress-custom-events.php';

			// Hooks.
			add_action( 'wp_enqueue_scripts', 'levup_enqueue', 100 );
			add_action( 'bp_loaded', 'register_shortcodes', 100 );
			add_action( 'wp_ajax_levup_filter_mentors', 'levup_filter_mentors' );
			add_action( 'wp_ajax_levup_request_mentorship', 'levup_request_mentorship' );
			add_action( 'wp_ajax_levup_add_mentorship_task', 'levup_add_mentorship_task' );
			add_action( 'wp_ajax_levup_task_process', 'levup_task_process' );
			add_action( 'wp_ajax_levup_auto_match', 'levup_auto_match' );
			add_action( 'wp_ajax_levup_update_mentorship_status', 'levup_update_mentorship_status' );
			add_action( 'wp_ajax_levup_upload_mentorship_file', 'levup_upload_mentorship_file' );
			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			add_filter( 'gamipress_activity_triggers', 'levup_register_custom_events' );
			add_action( 'user_register', 'levup_add_user_meta', 10, 1 );
			add_action(
				'wp_loaded',
				function() {
					// levup_plugin_activate();
				}
			);

			// create_default_page('ccatbox', 'ChatBox', 'simple_content');
			// ShortCodes.
		}
	);



} else {

	add_action( 'admin_notices', 'gamipress_buddyboss_not_activated' );
}
