<?php

function check_mentorship_availability( $mentor_id, $mentee_id = false, $number = false ) {

	global $wpdb;

	if ( $number == true ) {
		$count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}mentorships where (mentor_id = {$mentor_id} OR mentee_id = {$mentor_id}) AND mentorship_started = '2' AND mentorship_completed != '2' AND mentorship_started != 3" );

		return $count;

	}

	if ( $mentee_id !== false ) {

		// $count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}mentorships where (mentor_id = {$mentor_id} AND mentee_id = {$mentee_id}) OR (mentor_id = {$mentee_id} AND mentee_id = {$mentor_id}) AND mentorship_started = '2' AND mentorship_completed != '2'" );

		$count_1 = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}mentorships where (mentor_id = {$mentor_id} AND mentee_id = {$mentee_id}) AND mentorship_completed != '2' AND mentorship_started != 3" );

		$count_2 = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}mentorships where (mentor_id = {$mentee_id} AND mentee_id = {$mentor_id}) AND mentorship_completed != '2' AND mentorship_started != 3" );
		return intval( $count_1 ) < 1 && intval( $count_2 ) < 1;
	}

	$count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}mentorships where (mentor_id = {$mentor_id} OR mentee_id = {$mentor_id}) AND mentorship_started = '2' AND mentorship_completed != '2' AND mentorship_started != 3" );

	// var_dump($count < 5, $count );

	return $count < 4;
}
