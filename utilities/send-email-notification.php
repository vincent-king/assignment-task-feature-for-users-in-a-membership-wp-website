<?php

function send_email_notification( $mentor_id, $mentee_id, $event, $data ) {
	 global $wpdb;

	// $user_id = get_current_user_id();

	$mentorship_id = $data['mentorship_id'];

	$query = "SELECT * FROM {$wpdb->prefix}mentorships where mentorship_id = {$mentorship_id}";

	$mentorship = $wpdb->get_results( $query, ARRAY_A );

	$mentorship = $mentorship[0];
	$mentor     = get_user_by( 'ID', $mentor_id );

	$mentee = get_user_by( 'ID', $mentee_id );

	$mentorship_description = $mentorship['intro_message'];
	$mentorship_timeline    = $mentorship['timeline'];
	$mentorship_name        = $mentorship['mentorship_name'];
	$type_of_mentorship     = maybe_unserialize( $mentorship['type_of_mentorship'] );
	$tasks                  = maybe_unserialize( $mentorship['tasks'] );
	$files_and_urls         = maybe_unserialize( $mentorship['files_and_urls'] );

	if ( $event == 'mentorship-requested' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'mentorship-accepted' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'mentorship-rejected' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'mentorship-completed' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'task-completed' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'task-reverted' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'task-deleted' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

	if ( $event == 'file-uploaded' ) {
		$to = $mentor->data->user_email;

		$subject = '';

		$message = 'This is a Test Message';

		wp_mail( $to, $subject, $message );
	}

}
function set_html_content_type() {

	return 'text/html';
}
