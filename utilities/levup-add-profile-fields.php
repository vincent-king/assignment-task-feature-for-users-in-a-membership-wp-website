<?php


function levup_add_profile_group( $group ) {
	global $wpdb;

	$table = $wpdb->prefix . 'bp_xprofile_groups';

	if ( $group == 'Default' ) {
		$query   = "SELECT ID FROM $table where name = 'Details'";
		$results = $wpdb->get_results( $query );

		// //var_dump($results[0]->ID);

		return $results[0]->ID;

	}

	$checkIfExists = $wpdb->get_var( "SELECT ID FROM $table WHERE name=$group " );
	$query         = "SELECT * FROM $table WHERE name= '$group'";
	$query_results = $wpdb->get_results( $query );

	// //var_dump( $qu,  $group );

	if ( count( $query_results ) == 0 ) {
		$data = array(
			'name'        => $group,
			'description' => '',
			'group_order' => 0,
			'can_delete'  => 1,
		);

		$result = $wpdb->insert( $table, $data );

		$group_id = $wpdb->insert_id;

		if ( $result != false ) {
			return $group_id;
		} else {
			return false;
		}
	} else {
		return false;
	}

}

function levup_add_profile_field( $group_id, $field_name, $field_type, $field_order ) {
	global $wpdb;

	$table = $wpdb->prefix . 'bp_xprofile_fields';

	$query         = "SELECT * FROM $table WHERE name= '$field_name'";
	$query_results = $wpdb->get_results( $query );

	// //var_dump( $selected_mentorships );

	if ( count( $query_results ) == 0 ) {
		$data = array(
			'group_id'          => $group_id,
			'parent_id'         => 0,
			'type'              => $field_type,
			'name'              => $field_name,
			'description'       => '',
			'is_required'       => 0,
			'is_default_option' => 0,
			'field_order'       => $field_order,
			'option_order'      => 0,
			'order_by'          => 'custom',
			'can_delete'        => 1,
		);

		$result = $wpdb->insert( $table, $data );

		$field_id = $wpdb->insert_id;

		if ( $result != false ) {
			return $field_id;
		} else {
			return false;
		}
	} else {
		return false;
	}

}

function levup_add_profile_field_option( $group_id, $field_id, $option, $field_order ) {
	global $wpdb;

	$table = $wpdb->prefix . 'bp_xprofile_fields';

	// //var_dump( $selected_mentorships );

	$data = array(
		'group_id'          => $group_id,
		'parent_id'         => $field_id,
		'type'              => 'option',
		'name'              => $option,
		'description'       => '',
		'is_required'       => 0,
		'is_default_option' => 0,
		'field_order'       => $field_order,
		'option_order'      => 0,
		'order_by'          => 'custom',
		'can_delete'        => 1,
	);

	$result = $wpdb->insert( $table, $data );

	// $field_id = $wpdb->insert_id;

	if ( $result != false ) {
		return true;
	} else {
		return false;
	}

}
