<?php

function get_current_mentorships() {
	global $wpdb;

	$user_id = get_current_user_id();

	$query = "SELECT * FROM {$wpdb->prefix}mentorships where (mentor_id = {$user_id} OR mentee_id = {$user_id}) AND mentorship_completed != 2  AND mentorship_started != 3";

	$mentorships = $wpdb->get_results( $query, ARRAY_A );

	return $mentorships;

}

function get_user_threads( $user_id ) {
	 global $wpdb;

	// $user_id = get_current_user_id();

	$query = "SELECT thread_id FROM {$wpdb->prefix}bp_messages_recipients where user_id=$user_id";

	// var_dump($query);

	$user_threads = $wpdb->get_results( $query, ARRAY_A );

	$user_threads = array_map(
		function( $value ) {
			return $value['thread_id'];

		},
		$user_threads
	);
	return $user_threads;

}

function compareDeepValue( $val1, $val2 ) {
	 return strcmp( $val1['value'], $val2['value'] );
}
