<?php

function get_mentorship_history() {

	global $wpdb;

	$current_user_id = get_current_user_id();

		$mentors = $wpdb->get_results( "SELECT mentor_id FROM {$wpdb->prefix}mentorships where mentor_id != {$current_user_id}  AND mentorship_started = '2' AND mentorship_completed = '2'", ARRAY_A );
		$mentee  = $wpdb->get_results( "SELECT mentee_id FROM {$wpdb->prefix}mentorships where mentee_id != {$current_user_id}  AND mentorship_started = '2' AND mentorship_completed = '2'", ARRAY_A );

		return array(
			'mentors' => $mentors,
			'mentee'  => $mentee,
		);

}
