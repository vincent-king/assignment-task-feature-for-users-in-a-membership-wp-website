<?php

function create_default_page( $post_name, $post_title, $content ) {

	global $wpdb;

	if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = '$post_name'", 'ARRAY_A' ) ) {

		$current_user = wp_get_current_user();

		// create post object
		$page = array(
			'post_title'   => $post_title,
			'post_status'  => 'publish',
			'post_author'  => $current_user->ID,
			'post_type'    => 'page',
			'post_content' => $content,
		);

		// //var_dump($page);

		// insert the post into the database
		$result = wp_insert_post( $page );

		return $result;
	}

}
