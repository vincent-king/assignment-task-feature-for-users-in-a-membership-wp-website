<?php
	/**
	 * Shows the Error Message
	 *
	 * @return void
	 */
function gamipress_buddyboss_not_activated() {
	$class   = 'notice notice-error';
	$message = __( 'Assignment/ task feature Plugin requires the Gamipress, BuddyBoss, BuddyBoss Pro Plugin and Theme.', 'levup' );

	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
}
