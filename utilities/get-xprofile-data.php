<?php

function get_xprofile_data( $user_id ) {
	$data = array();

	// Get XProfile groups, only if the component is active.
	if ( bp_is_active( 'xprofile' ) ) {
		$fields_endpoint = new BP_REST_XProfile_Fields_Endpoint();

		$groups = bp_xprofile_get_groups(
			array(
				'user_id'                        => $user_id,
				'fetch_fields'                   => true,
				'fetch_field_data'               => true,
				'hide_empty_fields'              => true,
				'repeater_show_main_fields_only' => false,
			)
		);

		foreach ( $groups as $group ) {
			/*
			  $data['groups'][ $group->id ] = array(
				'name' => $group->name,
			);
			*/
			foreach ( $group->fields as $item ) {

				/**
				 * Added support for display name format support from platform.
				 */
				// Get the current display settings from BuddyBoss > Settings > Profiles > Display Name Format.
				if ( function_exists( 'bp_core_hide_display_name_field' ) && true === bp_core_hide_display_name_field( $item->id ) ) {
					continue;
				}

				if ( function_exists( 'bp_member_type_enable_disable' ) && false === bp_member_type_enable_disable() ) {
					if ( function_exists( 'bp_get_xprofile_member_type_field_id' ) && bp_get_xprofile_member_type_field_id() === $item->id ) {
						continue;
					}
				}
				/**
				 * --Added support for display name format support from platform.
				 */

				$field_value = $item->data->value;

				$data['groups'][ $group->name ]['fields'][ $item->name ] = array(
					'raw'          => $fields_endpoint->get_profile_field_raw_value( $field_value, $item ),
					'unserialized' => $fields_endpoint->get_profile_field_unserialized_value( $field_value, $item ),
					'rendered'     => $fields_endpoint->get_profile_field_rendered_value( $field_value, $item ),
				);
			}
		}
	} else {
		$data = array( __( 'No extended profile data available as the component is inactive', 'buddyboss' ) );
	}

	return $data;
}
