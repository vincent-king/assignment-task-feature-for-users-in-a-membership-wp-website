<?php

function register_shortcodes() {

	require LEVUP_DIR_PATH . 'components/general/header.php';
	require LEVUP_DIR_PATH . 'components/general/user-welcome.php';

	require LEVUP_DIR_PATH . 'shortcodes/user-dashboard.php';
	require LEVUP_DIR_PATH . 'shortcodes/find-a-mentor.php';
	require LEVUP_DIR_PATH . 'shortcodes/request-a-mentorship.php';
	require LEVUP_DIR_PATH . 'shortcodes/levup-chat-box.php';

	add_shortcode( 'display_user_dashboard', 'display_user_dashboard' );
	add_shortcode( 'find_a_mentor', 'find_a_mentor' );
	add_shortcode( 'request_a_mentorship', 'request_a_mentorship' );
	add_shortcode( 'levup_chat_box', 'levup_chat_box' );

}
