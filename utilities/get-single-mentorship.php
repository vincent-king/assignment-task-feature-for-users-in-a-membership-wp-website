<?php

function get_single_mentorship( $mentor_id, $mentee_id ) {
	global $wpdb;

	// $user_id = get_current_user_id();

	$query = "SELECT * FROM {$wpdb->prefix}mentorships where mentor_id = {$mentor_id} AND mentee_id = {$mentee_id} AND mentorship_completed != '2'";

	$mentorships = $wpdb->get_results( $query, ARRAY_A );

	return $mentorships;

}
