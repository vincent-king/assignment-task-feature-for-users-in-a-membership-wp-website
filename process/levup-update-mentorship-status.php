<?php

function levup_update_mentorship_status() {

	$response = array( 'status' => 1 );

	if ( ! isset( $_POST['mentorship_action'] ) || ! isset( $_POST['mentorship_id'] ) ) {
		wp_send_json( $response );
	}
	global $wpdb;

	$action             = $_POST['mentorship_action'];
	$mentorship_id      = $_POST['mentorship_id'];
	$mentor_id          = $_POST['mentor_id'];
	$mentee_id          = $_POST['mentee_id'];
	$type_of_mentorship = $_POST['mentorship_data']['type_of_mentorship'];

	if ( $action == 'accept' ) {
		$value = array( 'mentorship_started' => 2 );

		send_email_notification( $mentor_id, $mentee_id, 'mentorship-accepted', array( 'mentorship_id' => $mentorship_id ) );

	} elseif ( $action == 'reject' ) {
		$value = array( 'mentorship_started' => 3 );
		send_email_notification( $mentor_id, $mentee_id, 'mentorship-rejected', array( 'mentorship_id' => $mentorship_id ) );
	} elseif ( $action == 'complete' ) {
		$value = array( 'mentorship_completed' => 2 );

		foreach ( $type_of_mentorship as $mentorship ) {

			$mentorship_name = strtolower( $mentorship );

			$event = 'levup_' . $mentorship_name . '_event';

			// $custom_triggers['levup_' . $mentorship_name . 'event'] =  $value->name . ' Mentorship Completed Event' ;

			gamipress_trigger_event(
				array(
					// Mandatory data, the event triggered and the user ID to be awarded
					'event'   => $event,
					'user_id' => $mentee_id,
				// Also, you can add any extra parameters you want
				// They will be passed too on any hook inside the GamiPress awards engine
				// 'date' => date( 'Y-m-d H:i:s' ),
				// 'custom_param' => 'custom_value',
				)
			);

		}

		send_email_notification( $mentor_id, $mentee_id, 'mentorship-completed', array( 'mentorship_id' => $mentorship_id ) );
	}

	$table_name = $wpdb->prefix . 'mentorships';

	$where = array( 'mentorship_id' => $mentorship_id ); // NULL value in WHERE clause.

	// $serialized_values = maybe_serialize( $current_tasks );

	// $result = $wpdb->update( $table_name, $data, $where ); // Also works in this case.

	$result = $wpdb->update(
		$table_name,
		$value,
		$where
	);
	if ( $result != false ) {
		$response = array( 'status' => 2 );
	}

	wp_send_json( $response );

}
