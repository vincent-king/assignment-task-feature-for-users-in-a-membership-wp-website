<?php
/**
 *
 */
function levup_filter_mentors() {

	// check_ajax_referer( 'levup_filter_mentors_nonce' );

	$response = array( 'status' => 1 );

	if ( ! isset( $_POST['search'] ) ) {
		wp_send_json( $response );
	}
	$request_mentorship_page = home_url( 'request-a-mentorship' );

	$search = wp_unslash( $_POST['search'] );
	// $industry                    = wp_unslash( $_POST['industry'] );
	// $mentorship_level_required   = wp_unslash( $_POST['mentorship_level'] );
	// $mentorship_offered_required = wp_unslash( $_POST['mentorship_type'] );
	$args = array();

	$defaults = array(
		'type'   => 'thumb',
		'width'  => false,
		'height' => false,
		'class'  => 'avatar',
		'id'     => false,
		'alt'    => sprintf( __( 'Profile photo of %s', 'buddyboss' ), $fullname ),
	);
	$r        = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	$users_content_html = '';
	$users              = get_users(
		array(
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key'     => 'first_name',
					'value'   => $search,
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'last_name',
					'value'   => $search,
					'compare' => 'LIKE',
				),
			),
		)
	);

	foreach ( $users as $user ) {

		$groups = get_xprofile_data( $user->ID );

		$mentorship_offered = '';

		$name                         = $groups['groups']['Details']['fields']['First Name']['rendered'] == null ? 'No Name' : $groups['groups']['Details']['fields']['First Name']['rendered'];
		$profession                   = $groups['groups']['Details']['fields']['Profession']['raw'] == null ? 'Profession Unavailable' : $groups['groups']['Details']['fields']['Profession']['rendered'];
		$experience_level             = $groups['groups']['Mentor Ship']['fields']['Experience Level']['raw'] == null ? 'Experience Level Unavailable' : $groups['groups']['Mentor Ship']['fields']['Experience Level']['rendered'];
		$more_details                 = $groups['groups']['Professional']['fields']['More Details']['raw'] == null ? 'Details to be included' : $groups['groups']['Professional']['fields']['More Details']['rendered'];
		$user_mentor_level            = $groups['groups']['Details']['fields']['Profile Type']['raw'] == null ? 'Details to be included' : $groups['groups']['Details']['fields']['Profile Type']['raw'];
		$user_mentorship_offered_type = $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'] == null ? array() : $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'];
		$user_industry                = $groups['groups']['Mentor Ship']['fields']['Industry']['raw'] == null ? 'Details to be included' : maybe_unserialize( $groups['groups']['Mentor Ship']['fields']['Industry']['raw'] );

		$user_availability = check_mentorship_availability( $user->ID ) ? 'style="display:none"' : '';
		$start_mentorship  = check_mentorship_availability( $user->ID ) ? '' : 'style="display:none"';

		// var_dump($groups['groups']['Mentor Ship']['fields']['Industry']);

		if ( $user->ID == get_current_user_id() ) {
			continue;
		}

		if ( $user_mentor_level != 'Mentor' && $user_mentor_level != 'Master' ) {
			continue;
		}

		if ( empty( $user_mentorship_offered_type ) ) {
			continue;
		}

		// var_dump($user_mentor_level, $user_mentor_level != 'Mentor' && $user_mentor_level != 'Master');

		// echo '<pre>';
		// var_dump($user_industry, $_POST['industry'], in_array( $user_industry, $_POST['industry'] ));
		// echo '</pre>';
		// var_dump($_POST['industry'], $user_industry, ! in_array( $user_industry, $_POST['industry'], true ));

		if ( isset( $_POST['industry'] ) && ! empty( $_POST['industry'] ) ) {

			if ( ! array_intersect( wp_unslash( $_POST['industry'] ), $user_industry ) == $_POST['industry'] ) {

				continue;

			}
		}

		// //var_dump( in_array( $user_mentor_level, $_POST['mentor_level'] ), $user_mentor_level, $_POST['mentor_level'] );

		if ( isset( $_POST['mentor_level'] ) && ! empty( $_POST['mentor_level'] ) ) {

			if ( ! in_array( strtolower( $user_mentor_level ), $_POST['mentor_level'] ) ) {

				continue;

			}
		}

		// //var_dump( $user_mentorship_offered_type, $_POST['mentorship_type'] );

		if ( isset( $_POST['mentorship_type'] ) && ! empty( $_POST['mentorship_type'] ) ) {

			if ( ! array_intersect( wp_unslash( $_POST['mentorship_type'] ), $user_mentorship_offered_type ) == $_POST['mentorship_type'] ) {

				continue;

			}
		}

		foreach ( $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'] as $offered ) {

			$mentorship_offered .= '<i class="bi bi-check-lg"></i><span> ' . $offered . '</span><br>';

		}
		/*
			$users_content_html .= '                        <div class="col-sm-6 mt-4">
		<div class="card1">
			<div class="cardbx">
				<div class="img12">
					' . bp_core_fetch_avatar(
				array(
					'item_id' => $user->ID,
					'type'    => $type,
					'alt'     => $alt,
					'css_id'  => $id,
					'class'   => $class,
					'width'   => $width,
					'height'  => $height,
					'email'   => $user->data->user_mail,
				)
			) . '
				</div>
				<div class="box7">
				<h5><a style="text-decoration: none;" href="' . ''. '" > ' . $name . '</a></h5>
					<h6>' . $profession . '</h6>
				</div>
				<a href="">' . $experience_level . '</a>
			</div>
			<p>' . $more_details . '</p>
			<h2>Offer</h2>
			' . $mentorship_offered . '
			<div class="btn19 text">
				<button class="btn">Start with Mentorship</button>
			</div>
			<h6 style="display:none;" class="text-dark text-center mt-4 mb-2">Fully Booked</h6>
		</div>
		</div>';

		};
		*/
		$users_content_html .= '                        <div class="col-sm-6 mt-4 h-100" style="min-height:100%;" >
<div class="card1 ">
	<div class="cardbx">
		<div class="img12">
			' . bp_core_fetch_avatar(
			array(
				'item_id' => $user->ID,
				'type'    => $type,
				'alt'     => $alt,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => $user->data->user_mail,
			)
		) . '
		</div>
		<div class="box7">
		<h5><a href="' . get_site_url() . '/members/' . $user->data->user_nicename . '" > ' . $name . '</a></h5>
			<h6>' . $profession . '</h6>
		</div>
		<a href="">' . $experience_level . '</a>
	</div>
	<p>' . $more_details . '</p>
	<h2>Offer</h2>
	' . $mentorship_offered . '
	<div class="btn19 text">
	<a class="btn" ' . $start_mentorship . ' href="' . $request_mentorship_page . '?mentor=' . $user->ID . '"> Start with Mentorship </a>
		<button style="display:none;" href="' . $request_mentorship_page . '?mentor=' . $user->ID . '" class="btn">Start with Mentorship</button>
	</div>
	<h6 ' . $user_availability . ' class="text-dark text-center mt-4 mb-2">Fully Booked</h6>
</div>
</div>';

	};

	$response = array(
		'results' => $users_content_html,
		'status'  => 2,
	);

	wp_send_json( $response );

}
