<?php

function levup_upload_mentorship_file() {
	$response = array( 'status' => 1 );

	$mentorship_id = $_POST['mentorship_id'];
	$mentor_id     = $_POST['mentor_id'];
	$mentee_id     = $_POST['mentee_id'];

	if ( isset( $_FILES['task_file'] ) ) {

		$upload_overrides = array( 'test_form' => false );

			$upload = wp_handle_upload( $_FILES['task_file'], $upload_overrides );

			$file_url = $upload['url'];

	}

	global $wpdb;

	$table_name = $wpdb->prefix . 'mentorships';

	$prepared_statement = $wpdb->prepare( "SELECT files_and_urls FROM {$table_name} WHERE  mentorship_id = %d", $mentorship_id );
	$current_files      = $wpdb->get_col( $prepared_statement );
	$current_files      = maybe_unserialize( $current_files[0] );

	$current_files[ basename( $file_url ) ] = $file_url;  // NULL value.

	$where = array( 'mentorship_id' => $mentorship_id ); // NULL value in WHERE clause.

	$serialized_values = maybe_serialize( $current_files );

	// $result = $wpdb->update( $table_name, $data, $where ); // Also works in this case.
	$result = $wpdb->update(
		$table_name,
		array(
			'files_and_urls' => $serialized_values,
		),
		$where
	);

	if ( $result != false ) {
		$response = array( 'status' => 2 );

		$response['all_files'] = $current_files;

		send_email_notification(
			$mentor_id,
			$mentee_id,
			'file-uploaded',
			array(
				'mentorship_id' => $mentorship_id,
				'files'         => $serialized_values,
			)
		);

	}

	wp_send_json( $response );

}
