<?php

function levup_task_process() {
	 $response = array( 'status' => 1 );

	$task_id       = sanitize_text_field( $_POST['task_id'] );
	$mentorship_id = intval( $_POST['mentorship_id'] );
	$mentor_id     = intval( $_POST['mentor_id'] );
	$mentee_id     = intval( $_POST['mentee_id'] );
	$process       = sanitize_text_field( $_POST['process'] );

	global $wpdb;

	$current_tasks = $wpdb->get_var( "SELECT tasks FROM {$wpdb->prefix}mentorships where mentorship_id ='" . $mentorship_id . "'" );
	$user_stats    = get_user_meta( get_current_user_id(), 'levup_user_stats', true );
	$current_tasks = maybe_unserialize( $current_tasks );

	if ( $process == 'task_complete' ) {
		if ( $_POST['checked'] === 'true' ) {

			$user_stats['tasks_completed']++;

			if ( strtotime( $current_tasks[ $task_id ]['task_date'] ) > time() ) {
				$user_stats['tasks_completed_on_time']++;

				send_email_notification(
					$mentor_id,
					$mentee_id,
					'task-completed',
					array(
						'task'          => $current_tasks[ $task_id ],
						'mentorship_id' => $mentorship_id,
					)
				);

			}

			$current_tasks[ $task_id ]['task_completed'] = 'YES';

		}
		if ( $_POST['checked'] === 'false' ) {

			$user_stats['tasks_completed']--;

			if ( strtotime( $current_tasks[ $task_id ]['task_date'] ) > time() ) {
				$user_stats['tasks_completed_on_time']--;

			}

			$current_tasks[ $task_id ]['task_completed'] = 'NO';

			send_email_notification(
				$mentor_id,
				$mentee_id,
				'task-reverted',
				array(
					'task'          => $current_tasks[ $task_id ],
					'mentorship_id' => $mentorship_id,
				)
			);

		}

		if ( $user_stats['tasks_completed'] < 0 ) {
			$user_stats['tasks_completed'] = 0;
		}

		if ( $user_stats['tasks_completed_on_time'] < 0 ) {
			$user_stats['tasks_completed_on_time'] = 0;
		}
	}

	if ( $process == 'task_delete' ) {

		unset( $current_tasks[ $task_id ] );

		send_email_notification(
			$mentor_id,
			$mentee_id,
			'task-deleted',
			array(
				'task'          => $current_tasks[ $task_id ],
				'mentorship_id' => $mentorship_id,
			)
		);

	}

	// //var_dump($current_tasks);
	$table_name = $wpdb->prefix . 'mentorships';

	$where = array( 'mentorship_id' => $mentorship_id ); // NULL value in WHERE clause.

	$serialized_values = maybe_serialize( $current_tasks );

	// $result = $wpdb->update( $table_name, $data, $where ); // Also works in this case.
	$result = $wpdb->update(
		$table_name,
		array(
			'tasks' => $serialized_values,
		),
		$where
	);

	update_user_meta( get_current_user_id(), 'levup_user_stats', $user_stats );

	if ( $result != false ) {
		$response = array( 'status' => 2 );

		$response['tasks'] = $current_tasks;
	}

	wp_send_json( $response );

}
