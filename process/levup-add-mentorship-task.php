<?php
/**
 * Adds the Mentorship Task.
 */
function levup_add_mentorship_task() {
	check_ajax_referer( 'levup_add_task_nonce' );

	$response = array( 'status' => 1 );

	if ( ! isset( $_POST['task_name'], $_POST['mentorship_id'], $_POST['task_date'], $_POST['task_description'], $_POST['task_assigned_to'] ) ) {
		wp_send_json( $response );
	}
	$args = [];

	$defaults = array(
		'type'   => 'thumb',
		'width'  => 30,
		'height' => 30,
		'class'  => 'avatar',
		'id'     => false,
	);
	$r        = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	$task_name        = sanitize_text_field( $_POST['task_name'] );
	$mentorship_id    = intval( $_POST['mentorship_id'] );
	$task_date        = $_POST['task_date'];
	$task_description = sanitize_textarea_field( $_POST['task_description'] );
	$task_assigned_to = sanitize_text_field( $_POST['task_assigned_to'] );

	global $wpdb;

	$table_name = $wpdb->prefix . 'mentorships';

	$prepared_statement = $wpdb->prepare( "SELECT tasks FROM {$table_name} WHERE  mentorship_id = %d", $mentorship_id );
	$current_tasks      = $wpdb->get_col( $prepared_statement );
	$current_tasks      = maybe_unserialize( $current_tasks[0] );

	$key = time();

	if ( $task_assigned_to == 'mentor' ) {

		$asignee_url = bp_core_fetch_avatar(
			array(
				'item_id' => $_POST['mentor_id'],
				'type'    => $type,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => '',
			)
		);

		$assignee_id = $_POST['mentor_id'];
	} elseif ( $task_assigned_to == 'mentee' ) {
		$asignee_url = bp_core_fetch_avatar(
			array(
				'item_id' => $_POST['mentee_id'],
				'type'    => $type,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => '',
			)
		);

	}

	$current_tasks[ $key ] = array(
		'task_name'        => $task_name,
		'task_date'        => $task_date,
		'task_description' => $task_description,
		'task_assigned_to' => $task_assigned_to,
		'task_completed'   => false,
		'assignee_url'     => $asignee_url,
	); // NULL value.
	$where                 = array( 'mentorship_id' => $mentorship_id ); // NULL value in WHERE clause.

	$serialized_values = maybe_serialize( $current_tasks );

	// $result = $wpdb->update( $table_name, $data, $where ); // Also works in this case.
	$result = $wpdb->update(
		$table_name,
		array(
			'tasks' => $serialized_values,
		),
		$where
	);

	if ( $result != false ) {
		$response = array( 'status' => 2 );

		$response['all_tasks'] = $current_tasks;

		$response['task_added'] = array(
			'key'  => $key,
			'task' => $current_tasks[ $key ],
		);
	}

	wp_send_json( $response );
}
