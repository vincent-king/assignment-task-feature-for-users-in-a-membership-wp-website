<?php

/**
 *
 */
function levup_request_mentorship() {

	check_ajax_referer( 'levup_request_mentorship_nonce' );

	$response = array( 'status' => 1 );

	if ( ! isset( $_POST['mentor_id'], $_POST['mentorships_selected'], $_POST['timeline'], $_POST['message'] ) ) {
		wp_send_json( $response );
	}

	$mentor_id            = intval( $_POST['mentor_id'] );
	$mentee_id            = get_current_user_id();
	$selected_mentorships = array_map( 'sanitize_key', $_POST['mentorships_selected'] );
	$timeline             = $_POST['timeline'];
	$message              = sanitize_textarea_field( wp_unslash( $_POST['message'] ) );
	
	$current_mentorships_count_mentor = check_mentorship_availability( $mentor_id, false, true );
	$current_mentorships_count_mentee = check_mentorship_availability( $mentee_id, false, true );

	if ( ! check_mentorship_availability( $mentor_id ) && ! check_mentorship_availability( $mentee_id ) && $current_mentorships_count_mentor < 4 && $current_mentorships_count_mentee < 4) {
		wp_send_json( $response );

	}
	$current_mentorships_count = check_mentorship_availability( $current_user_id, false, true );

	global $wpdb;

	$table = $wpdb->prefix . 'mentorships';

	$mentor = get_user_by( 'ID', $mentor_id );
	$mentee = get_user_by( 'ID', $mentee_id );

	$mentorship_name = $mentor->data->display_name . ' and ' . $mentee->data->display_name . '\'s Mentorship';

	// //var_dump( $selected_mentorships );

	$timeline = new DateTime( $timeline );

	$timeline = $timeline->format( 'Y-m-d' );

	$data = array(
		'mentorship_name'      => $mentorship_name,
		'mentor_id'            => $mentor_id,
		'mentee_id'            => $mentee_id,
		'timeline'             => $timeline,
		'intro_message'        => $message,
		'tasks'                => array(),
		'mentorship_started'   => 1,
		'mentorship_completed' => 1,
		'type_of_mentorship'   => maybe_serialize( $selected_mentorships ),
	);

	 $result = $wpdb->insert( $table, $data );

	if ( $result !== false ) {
		$response = array( 'status' => 2 );

		send_email_notification( $mentor_id, $mentee_id, 'mentorship-requested', $data );

	}

	$response['data']      = $data;
	$response['user_data'] = get_xprofile_data( $mentor_id );

	wp_send_json( $response );
}
