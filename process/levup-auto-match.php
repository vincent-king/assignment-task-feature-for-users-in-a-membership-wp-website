<?php


function levup_auto_match() {

	check_ajax_referer( 'levup_filter_mentors_nonce' );

	$response = array( 'status' => 1 );

	if ( ! isset( $_POST['search'] ) ) {
		wp_send_json( $response );
	}

	$search = wp_unslash( $_POST['search'] );
	// $industry                    = wp_unslash( $_POST['industry'] );
	// $mentorship_level_required   = wp_unslash( $_POST['mentorship_level'] );
	// $mentorship_offered_required = wp_unslash( $_POST['mentorship_type'] );

	$users_content_html = '';
	$users              = get_users(
		array(
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key'     => 'first_name',
					'value'   => $search,
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'last_name',
					'value'   => $search,
					'compare' => 'LIKE',
				),
			),
		)
	);

	$final_users = array();

	foreach ( $users as $user ) {

		if ( $user->ID == get_current_user_id() ) {
			continue;
		}

		$groups = get_xprofile_data( $user->ID );

		$mentorship_offered = '';

		$name                         = $groups['groups']['Details']['fields']['First Name']['raw'] == null ? 'No Name' : $groups['groups']['Details']['fields']['First Name']['rendered'];
		$profession                   = $groups['groups']['Details']['fields']['Profession']['raw'] == null ? 'Profession Unavailable' : $groups['groups']['Details']['fields']['Profession']['rendered'];
		$experience_level             = $groups['groups']['Mentor Ship']['fields']['Experience Level']['raw'] == null ? 'Experience Level Unavailable' : $groups['groups']['Mentor Ship']['fields']['Experience Level']['rendered'];
		$more_details                 = $groups['groups']['Professional']['fields']['More Details']['raw'] == null ? 'Details to be included' : $groups['groups']['Professional']['fields']['More Details']['rendered'];
		$user_mentor_level            = $groups['groups']['Details']['fields']['Profile Type']['raw'] == null ? 'Details to be included' : $groups['groups']['Details']['fields']['Profile Type']['raw'];
		$user_mentorship_offered_type = $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'] == null ? array() : $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'];
		$user_industry                = $groups['groups']['Mentor Ship']['fields']['Industry']['raw'] == null ? 'Details to be included' : $groups['groups']['Mentor Ship']['fields']['Industry']['raw'];

		if ( $user_mentor_level != 'Mentor' && $user_mentor_level != 'Master' ) {
			continue;
		}

		if ( isset( $_POST['industry'] ) && ! empty( $_POST['industry'] ) ) {

			if ( ! in_array( $user_industry, $_POST['industry'], true ) ) {

				continue;

			}
		}

		// //var_dump( in_array( $user_mentor_level, $_POST['mentor_level'] ), $user_mentor_level, $_POST['mentor_level'] );

		if ( isset( $_POST['mentor_level'] ) && ! empty( $_POST['mentor_level'] ) ) {

			if ( ! in_array( strtolower( $user_mentor_level ), $_POST['mentor_level'] ) ) {

				continue;

			}
		}

		// //var_dump( $user_mentorship_offered_type, $_POST['mentorship_type'] );

		if ( isset( $_POST['mentorship_type'] ) && ! empty( $_POST['mentorship_type'] ) ) {

			if ( ! array_intersect( wp_unslash( $_POST['mentorship_type'] ), $user_mentorship_offered_type ) == $_POST['mentorship_type'] ) {

				continue;

			}
		}

		$availability = check_mentorship_availability( $user->ID ) && check_mentorship_availability( $user->ID, get_current_user_id() );

		if ( ! $availability ) {

			continue;

		}

		$final_users[] = $user->ID;

	}

		$response = array(
			'results' => $final_users,
			'status'  => 2,
		);

		wp_send_json( $response );

}
