<?php

/**
 * Enqueues Scripts and Styles.
 *
 * @return void
 */
function levup_enqueue() {

	wp_dequeue_script( 'bb-pro-pusher' );

	wp_register_style( 'levup2_style_css', plugins_url( 'assets/css/style.css', LEVUP_PATH ), array(), time() );
	// wp_register_style( 'levup2_stye2_css', plugins_url( 'assets/css/style2.css', LEVUP_PATH ), array(), time() );
	wp_register_style( 'levup2_media_css', plugins_url( 'assets/css/media.css', LEVUP_PATH ), array(), time() );
	// wp_register_style( 'levup2_media2_css', plugins_url( 'assets/css/media2.css', LEVUP_PATH ), array(), time() );
	wp_register_style( 'levup2_custom_css', plugins_url( 'assets/css/custom.css', LEVUP_PATH ), array(), time() );
	wp_register_style( 'levup2_bootstrap_icons', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css', array(), time() );
	wp_register_style( 'levup2_bootstrap_css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css', array(), time() );
	wp_register_style( 'levup2_font_awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css', array(), time() );
	wp_register_style( 'levup2_roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap', array(), time() );
	wp_register_style( 'levup2_montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap', array(), time() );
	wp_register_style( 'levup2_mdb_css', 'https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.3.1/mdb.min.css', array(), time() );
	wp_register_style( 'levup2_bootstrap_datetimepicker_css', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css', array(), time() );

	wp_register_script( 'levup2_bootstrap_bundle', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js', array(), time(), true );
	wp_register_script( 'levup2_mdb_js', 'https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.3.1/mdb.min.js', array(), time(), true );
	wp_register_script( 'levup2_moment_js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js', array(), time(), true );
	wp_register_script( 'levup2_popper_js', 'https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js', array(), time(), true );
	wp_register_script( 'levup2_bootstrap_js', 'https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js', array(), time(), true );
	wp_register_script( 'levup2_bootstrap_datepicker_js', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js', array(), time(), true );

	wp_register_script( 'levup2_main_js', plugins_url( 'assets/js/levup-main.js', LEVUP_PATH ), array( 'bp-nouveau', 'json2', 'wp-backbone', 'bp-nouveau-messages-at', 'bp-select2' ), time(), true );
	wp_register_script( 'bb-pro-pusher', plugins_url( 'assets/js/bb-pusher.min.js', LEVUP_PATH ), array( 'bb-pro-pusher-js-lib', 'wp-i18n', 'bb-pro-deparam-js', 'bb-pro-pusher-auth-js' ), time(), false );

	wp_enqueue_style( 'levup2_style_css' );
	wp_enqueue_style( 'levup2_media_css' );
	wp_enqueue_style( 'levup2_custom_css' );
	wp_enqueue_style( 'levup2_bootstrap_icons' );
	wp_enqueue_style( 'levup2_bootstrap_css' );
	wp_enqueue_style( 'levup2_font_awesome' );
	wp_enqueue_style( 'levup2_roboto' );
	wp_enqueue_style( 'levup2_montserrat' );
	wp_enqueue_style( 'levup2_mdb_css' );
	wp_enqueue_style( 'levup2_bootstrap_datetimepicker_css' );

	wp_enqueue_script( 'levup2_bootstrap_bundle' );
	wp_enqueue_script( 'levup2_mdb_js' );
	wp_enqueue_script( 'levup2_moment_js' );
	wp_enqueue_script( 'levup2_popper_js' );
	wp_enqueue_script( 'levup2_bootstrap_js' );
	wp_enqueue_script( 'levup2_bootstrap_datepicker_js' );
	wp_enqueue_script( 'levup2_main_js' );
	wp_enqueue_script( 'bb-pro-pusher' );

	$user_mentorship_data = get_current_mentorships();

	foreach ( $user_mentorship_data as $key => $value ) {

		$user_mentorship_data[ $key ]['type_of_mentorship'] = maybe_unserialize( $user_mentorship_data[ $key ]['type_of_mentorship'] );
		$user_mentorship_data[ $key ]['tasks']              = maybe_unserialize( $user_mentorship_data[ $key ]['tasks'] );
		$user_mentorship_data[ $key ]['files_and_urls']     = maybe_unserialize( $user_mentorship_data[ $key ]['files_and_urls'] );

		$mentor = get_user_by( 'ID', $user_mentorship_data[ $key ]['mentor_id'] );
		$mentee = get_user_by( 'ID', $user_mentorship_data[ $key ]['mentee_id'] );

		$user_mentorship_data[ $key ]['users'] = array(
			'mentor' => $mentor,
			'mentee' => $mentee,
		);

	}

	$message_nonce = wp_create_nonce( 'messages_send_message' );

	wp_localize_script(
		'levup2_main_js',
		'levup_obj',
		array(
			'ajax_url'             => admin_url( 'admin-ajax.php' ),
			'site_url'             => get_site_url(),
			'current_user_id'      => get_current_user_id(),
			'user_mentorship_data' => $user_mentorship_data,
			'nonces'               => array( 'message' => $message_nonce ),
			'plugin_image_url'     => plugins_url( 'assets', LEVUP_PATH ),
			'current_user'         => get_user_by( 'ID', get_current_user_id() ),

		)
	);

}
