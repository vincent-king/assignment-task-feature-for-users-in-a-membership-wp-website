<?php
/**
 * Adds the database as well as initial meta to the user.
 *
 * @return void
 */
function levup_plugin_activate() {

	$active_plugins = get_option( 'active_plugins' );

	$buddy_boss_exists = in_array( 'buddyboss-platform/bp-loader.php', $active_plugins, true );
	$gamipress_exists  = in_array( 'gamipress/gamipress.php', $active_plugins, true );

	if ( ! ( $buddy_boss_exists && $gamipress_exists ) ) {
		@trigger_error( __( 'Assignment/ task feature Plugin requires the Gamipress, BuddyBoss, BuddyBoss Pro Plugin and Theme.', 'levup' ), E_USER_ERROR );
	}

	global $wpdb;

	$database_query = 'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'mentorships` (
        `mentorship_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `mentorship_name` VARCHAR(50) NULL DEFAULT NULL ,
		`mentor_id` INT NULL,
		`mentee_id` INT NULL,
        `tasks` LONGTEXT NULL DEFAULT NULL ,
        `files_and_urls` MEDIUMTEXT NULL DEFAULT NULL ,
        `timeline` DATETIME NULL DEFAULT NULL,
        `intro_message` VARCHAR(1000) NULL DEFAULT NULL ,
        `mentorship_started` VARCHAR(50) NULL DEFAULT NULL ,
        `mentorship_completed` VARCHAR(50) NULL DEFAULT NULL ,
        `type_of_mentorship` VARCHAR(50) NULL DEFAULT NULL ,
        PRIMARY KEY (`mentorship_id`)
    ) ENGINE=InnoDB ' . $wpdb->get_charset_collate() . ';';

	require_once ABSPATH . '/wp-admin/includes/upgrade.php';

	dbDelta( $database_query );

	// Get all users.
	$wp_user_query = 'SELECT * FROM ' . $wpdb->prefix . 'users';

	// Get the results.
	$users = $wpdb->get_results( $wp_user_query );// wp no-cache ok.
	// Check for results.
	if ( ! empty( $users ) ) {

		// loop trough each user.
		foreach ( $users as $user ) {
			$user_meta = get_user_meta( $user->ID, 'levup_user_stats' );
			if ( empty( $user_meta ) ) {

				$levup_user_stats = array(
					'mentorships_completed_as_mentee' => 0,
					'mentorships_completed_as_mentor' => 0,
					'tasks_completed'                 => 0,
					'tasks_completed_on_time'         => 0,
				);

				add_user_meta( $user->ID, 'levup_user_stats', $levup_user_stats, true ); // adds user_meta with appropriate_info
			}
		}
	}

	$default_fields = array(
		'Default'      => array(
			'Profile Type' => array(
				'type'    => 'selectbox',
				'options' => array( 'Mentor', 'Master' ),
			),
			'Profession'   => array( 'type' => 'textbox' ),
		),
		'Mentor Ship'  => array(
			'Experience Level' => array(
				'type'    => 'selectbox',
				'options' => array( 'Gold', 'Silver', 'Bronze' ),
			),
			'Industry'         => array(
				'type'    => 'multiselectbox',
				'options' => array( 'Computer', 'IT', 'Education', 'Food' ),
			),
		),
		'Professional' => array(
			'Mentorship Type' => array(
				'type'    => 'multiselectbox',
				'options' => array( 'Computer', 'Science', 'Engineering', 'Literature' ),
			),
			'More Details'    => array( 'type' => 'textarea' ),
		),
	);

	foreach ( $default_fields as $group => $group_value ) {
		$group_id = levup_add_profile_group( $group );

		if ( $group_id != false ) {
			foreach ( $group_value as $key => $value ) {

				$field_id = levup_add_profile_field( $group_id, $key, $value['type'], 0 );

				// var_dump($field_id != false && isset($value['options']), $value['options']);

				if ( $field_id != false && isset( $value['options'] ) ) {
					$num = 1;
					foreach ( $value['options'] as $option ) {
						levup_add_profile_field_option( $group_id, $field_id, $option, $num );
						$num++;
					}
				}
			}
		} else {
			continue;
		}
	}

						$pages = array(
							array(
								'name'    => 'find-a-mentor',
								'title'   => 'Find a Mentor',
								'content' => '[find_a_mentor]',
							),

							array(
								'name'    => 'request-a-mentorship',
								'title'   => 'Request a Mentorship',
								'content' => '[request_a_mentorship]',
							),

							array(
								'name'    => 'user-dashboard',
								'title'   => 'User Dashboard',
								'content' => '[display_user_dashboard]',
							),

							array(
								'name'    => 'chatbox',
								'title'   => 'ChatBox',
								'content' => '[levup_chat_box]',
							),
						);

						foreach ( $pages as $page ) {

							create_default_page( $page['name'], $page['title'], $page['content'] );

						}

}


function levup_add_user_meta( $user_id ) {
	$user_meta = get_user_meta( $user_id, 'levup_user_stats' );
	if ( empty( $user_meta ) ) {

		$levup_user_stats = array(
			'mentorships_completed_as_mentee' => 0,
			'mentorships_completed_as_mentor' => 0,
			'tasks_completed'                 => 0,
			'tasks_completed_on_time'         => 0,
		);

		add_user_meta( $user_id, 'levup_user_stats', $levup_user_stats, true ); // adds user_meta with appropriate_info
	}

}
