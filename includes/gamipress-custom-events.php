<?php

function levup_register_custom_events( $triggers ) {
	global $wpdb;

	$mentorship_type_id = $wpdb->get_var( "SELECT ID FROM {$wpdb->prefix}bp_xprofile_fields where name='Mentorship Type'" );

	$mentorship_type_info = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}bp_xprofile_fields where parent_id='{$mentorship_type_id}'" );

	foreach ( $mentorship_type_info as $key => $value ) {

		$mentorship_name = strtolower( $value->name );

		$custom_triggers[ 'levup_' . $mentorship_name . '_event' ] = $value->name . ' Mentorship Completed Event';

	};

	$triggers['Levup Custom Events'] = $custom_triggers;

	// var_dump($triggers);

	return $triggers;

}
