
window.bp = window.bp || {};
window.Backbone = window.Backbone || [];

(function ($) {

    var current_mentorship_data;
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    _.extend(bp, _.pick(wp, 'Backbone', 'ajax', 'template'));

    console.log(bp);

    // Add Color Picker to all inputs that have 'color-field' class
    $('#levup-filter-mentors').on('submit', function (e) {
        e.preventDefault();

        var data = $('#levup-filter-mentors').serialize();

        $.post(levup_obj.ajax_url, data,
            function (data) {
                if (data.status == 2) {
                    $('#mentors-results').html(data.results);
                }
            }
        );

    })
    var nonce = BP_Nouveau;

    console.log(nonce);

    $('#request-a-mentorship-form').on('submit', function (e) {
        e.preventDefault();

        var data = $('#request-a-mentorship-form').serialize();

        $.post(levup_obj.ajax_url, data,
            function (response) {
                if (response.status == 2) {

                    //         var nonce = BP_Nouveau.messages.nonces.send;

                    if (response['user_data']['groups']['Details']['fields']['Nickname']['raw'] == null) {
                        return;
                    }
                    var user_nickname = response['user_data']['groups']['Details']['fields']['Nickname']['raw'] == null ? 'No Name' : response['user_data']['groups']['Details']['fields']['Nickname']['raw'];
                    var first_name = response['user_data']['groups']['Details']['fields']['First Name']['raw'] == null ? 'No Name' : response['user_data']['groups']['Details']['fields']['First Name']['raw'];

                    console.log(response['user_data']['groups']['Details']['fields']['Nickname']);

                    var data = {
                        nonce: levup_obj.nonces.message,
                        send_to: ['@' + user_nickname],
                        subject: '',
                        message_content: response['data']['intro_message'],
                        sending: true,
                        action: 'messages_send_message'
                    }

                    console.log(data);

                    $.post(levup_obj.ajax_url, data, function (response) {
                        console.log(response);

                        if (response.success == true) {
                            document.location.href = levup_obj.site_url + '/chatbox/?thread=' + response.data.thread_id;
                        }
                    }

                    )
                }
            }
        );

    })
    function isEmpty(obj) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    var threads_nonce = $('#user_threads_nonce');

    console.log(levup_obj.user_mentorship_data);

    if (document.getElementById('user_threads_nonce')) {
        var data = {
            box: "single",
            nonce: $('#user_threads_nonce')[0].value,
            thread_type: "unarchived",
            action: "messages_get_user_message_threads"
        }

        var user_messages_threads = $.post(levup_obj.ajax_url, data,
            function (response) {
                var current_user_id = levup_obj.current_user_id;
                var threads = response.data.threads;
                var mentorship_text = '';
                var peer_text = '';

                console.log(threads);
                var mentorship_mentor_mentee_ids = [];

                levup_obj.user_mentorship_data.forEach(element => {
                    mentorship_mentor_mentee_ids.push(element.mentee_id, element.mentor_id);

                });
                var mentorship_mentor_mentee_ids = [...new Set(mentorship_mentor_mentee_ids)];

                console.log(mentorship_mentor_mentee_ids);

                threads.forEach(thread => {
                    console.log(thread);
                    var recipient_id = thread.recipients[1].id;
                    var recipient_name = thread.recipients[1].user_name;
                    var user_link = thread.recipients[1].user_link;
                    var avatar = '<img src="' + thread.recipients[1].avatar + '" class="avatar user-2-avatar avatar-150 photo" width="150" height="150" alt="Profile photo of ">';
                    var thread_id = thread.id;

                    var current_mentorship_data = levup_obj.user_mentorship_data.filter(function (element) {

                        return (element.mentee_id == current_user_id && element.mentor_id == recipient_id) || (element.mentee_id == recipient_id && element.mentor_id == current_user_id);

                    });

                    console.log(current_mentorship_data);



                    console.log(recipient_id)

                    console.log(mentorship_mentor_mentee_ids.includes(recipient_id.toString()));

                    if (mentorship_mentor_mentee_ids.includes(recipient_id.toString())) {
                        var relationship = '';
                        if (current_mentorship_data[0].mentor_id == recipient_id) {
                            relationship = 'Mentor';
                        }
                        else if (current_mentorship_data[0].mentee_id == recipient_id) {
                            relationship = 'Mentee';
                        }



                        mentorship_text += ' <a class="bp-message-link levup-message-link" data-recipient-name="' + recipient_name + '" data-recipient-id="' + recipient_id + '" data-avatar-src=' + thread.recipients[1].avatar + ' data-thread-id=' + thread_id + '>     <div class=" part11 bdright borderBottom"  class="">' + avatar + '<span class="sergment">' + relationship + '</span><div class="sertext">' + recipient_name + '</div></div></a>';
                    }
                    else {
                        peer_text += ' <a class="bp-message-link levup-message-link" data-recipient-name="' + recipient_name + '" data-recipient-id="' + recipient_id + '" data-avatar-src=' + thread.recipients[1].avatar + ' data-thread-id=' + thread_id + '>     <div class=" part11 bdright borderBottom"  class="">' + avatar + '<div class="sertext">' + recipient_name + '</div></div></a>';
                    }



                });

                $('#mentorship-chat-text').html(mentorship_text);
                $('#peers-chat-text').html(peer_text);

                $('.levup-message-link').click(function () {
                    var thread_id = $(this).attr("data-thread-id");
                    var thread_user_img_src = $(this).attr("data-avatar-src");
                    var recipient_id = $(this).attr("data-recipient-id");
                    var recipient_name = $(this).attr("data-recipient-name");

                    const url = new URL(window.location);
                    url.searchParams.set('thread', thread_id);
                    window.history.pushState(null, '', url.toString());
                    $('#message').attr("data-thread-id", thread_id);

                    var data = {
                        id: thread_id,
                        nonce: $('#user_threads_nonce')[0].value,
                        action: "messages_get_thread_messages",
                        before: '',
                        thread_type: "unarchived"
                    }

                    $.post(levup_obj.ajax_url, data,
                        function (response) {

                            var threads_text = '';

                            console.log(response);

                            var messages = response.data.messages;
                            var d = new Date(0); // The 0 there is the key, which sets the date to the epoch

                            messages.reverse();

                            messages.forEach(message => {
                                threads_text += '<li class=""> <div class="bp-single-message-wrap"> <div class="bp-avatar-wrap"> <a href="' + message.sender_link + '" class="bp-user-avatar"> <img class="avatar" src="' + message.sender_avatar + '" alt=""> </a> </div> <div class="bp-single-message-content"> <div class="message-metadata"> <a href="' + message.sender_link + '" class="bp-user-link"> <strong>' + message.sender_name + '</strong> </a> <time datetime="' + d.setUTCSeconds(message.date) + '" class="activity">' + message.display_date + '</time> </div> <div class="bp-message-content-wrap"><p>' + message.content + '</p> </div> ';

                                if (message.media != undefined) {
                                    threads_text += '<div class="bb-activity-media-wrap bb-media-length-1">';
                                    message.media.forEach(element => {

                                        threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="' + element.privacy + '" href="' + element.full + '"> <img  src="' + element.thumbnail + '" alt="' + element.thumbnail + '"> </a> </div> '

                                    });
                                    threads_text += '</div>';
                                }
                                if (message.video != undefined) {
                                    console.log(message.video_html)

                                    threads_text += '<div class="bb-activity-video-wrap bb-video-length-1 embed-responsive" >'

                                    message.video.forEach(element => {

                                        //   threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="'+ element.privacy +'" href="#"> <img  src="' + element.thumbnail + '" alt="'+ element.thumbnail +'"> </a> </div> ';

                                        element.video_html = element.video_html.replace('<video', '<video style="width:100%;height:100%; "');

                                        threads_text += element.video_html;

                                    });

                                    threads_text += '</div>'
                                }
                                if (message.document != undefined) {
                                    console.log(message.document)

                                    threads_text += '<div style="width:90%;" class="bb-activity-media-wrap bb-media-length-1 embed-responsive" >'

                                    message.document.forEach(element => {
                                        console.log(element)

                                        threads_text += '<div style="width:90%;"  class="bb-activity-media-elem document-activity " data-id="' + element.id + ' "> <div class="document-description-wrap"> <a href="' + element.url + ' " class="entry-img" data-id="4" data-activity-id="4"> <i class="bb-icon-file-pdf"></i> </a> <a href="' + element.url + ' " class="document-detail-wrap bb-open-document-theatre" data-id="' + element.id + ' " data-activity-id="" data-icon-class="bb-icon-file-pdf&quot;" data-attachment-id="' + element.attachment_id + ' " data-attachment-full="" data-privacy="message" data-extension="' + element.extension + ' " data-author="' + element.author + ' " data-preview="" data-full-preview="" data-text-preview="' + element.text_preview + ' " data-mp3-preview="" data-document-title="' + element.document_title + ' " data-video-preview="" data-mirror-text=""> <span class="document-title">' + element.document_title + ' </span> <span class="document-description">' + element.size + ' </span> <span class="document-extension-description">' + element.extension + ' </span> <span class="document-helper-text"> <span> - </span> <span class="document-helper-text-inner">Click to view</span></span> </a> </div> <div class="document-action-wrap"> <a href="#" class="document-action_collapse" data-balloon-pos="up" data-tooltip-collapse="Collapse" data-balloon="Expand"><i class="bb-icon-merge bb-icon-l document-icon-collapse"></i></a> <a href="' + element.url + ' " class="document-action_download" data-balloon-pos="up" data-balloon="Download"> <i class="bb-icon-l bb-icon-download"></i> </a> <a href="#" target="_blank" class="document-action_more" data-balloon-pos="up" data-balloon="More actions"> <i class="bb-icon-f bb-icon-ellipsis-h"></i> </a> <div class="document-action_list"> <ul> <li class="copy_download_file_url"> <a href="' + element.url + ' ">Copy Download Link</a> </li> </ul> </div> </div> </div>';

                                    });

                                    threads_text += '</div>'
                                }

                                threads_text += '</div> </div> </li>';

                            });




                            $('#levup-message-thread-list').html(threads_text);




                            var current_mentorship_data = levup_obj.user_mentorship_data.filter(function (element) {

                                return (element.mentee_id == current_user_id && element.mentor_id == recipient_id) || (element.mentee_id == recipient_id && element.mentor_id == current_user_id);

                            });

                            if (current_mentorship_data.length != 0 && current_mentorship_data[0].mentorship_started == "2") {

                                if (current_mentorship_data[0].mentor_id == current_user_id) {
                                    relationship = 'Mentee';
                                }
                                else if (current_mentorship_data[0].mentee_id == current_user_id) {
                                    relationship = 'Mentor';
                                }


                                console.log(current_mentorship_data, recipient_id, current_user_id);

                                $('.sergey').html('<img src="' + thread_user_img_src + '" style="width:60px;height:auto;"> <span class="span">' + relationship + '</span>');
                                $('#levup-mentorship-title').html(current_mentorship_data[0].mentorship_name);
                                $('#mentorship_id')[0].value = current_mentorship_data[0].mentorship_id;
                                $('#mentor_id')[0].value = current_mentorship_data[0].mentor_id;
                                $('#mentee_id')[0].value = current_mentorship_data[0].mentee_id;



                                var type_of_mentorship_text = '';

                                var type_of_mentorship_array = current_mentorship_data[0].type_of_mentorship;

                                type_of_mentorship_array.forEach(element => {

                                    type_of_mentorship_text += '<span class="license">' + element + ' </span>';

                                });

                                $('#type-of-mentorships').html(type_of_mentorship_text);
                                var mentorships_tasks_text = '';

                                var mentorships_tasks = current_mentorship_data[0].tasks;

                                if (current_mentorship_data[0].mentor_id != current_user_id) {
                                    $('#finish-mentorship').hide();

                                    $("#levup-add-task-form").hide();

                                    $('a .task-delete-chk').hide();

                                }
                                else {

                                    $("#levup-add-task-form").show();

                                }

                                var mentorship_task_completed = 0;
                                var mentorships_all_tasks = 0;

                                if (mentorships_tasks.length != 0) {
                                    for (const key in mentorships_tasks) {
                                        var checked = mentorships_tasks[key].task_completed == 'YES' ? 'checked' : '';

                                        var task_delete = current_mentorship_data[0].mentor_id != current_user_id ? 'style="display:none"' : ''



                                        //  var task_delete = true ? '<a  class="task-delete-chk button" data-id="'+ key + '" style="background:transparent;border:none;" ><i class="fa-regular fa-trash-can" style="color: #bf4040;"></i></a>'  : '';

                                        var assignee_url_html = mentorships_tasks[key].assignee_url;

                                        //   var assignee_url_match = assignee_url_html.match(/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/)

                                        var assignee_url = '';

                                        if (assignee_url_html != null) {
                                            console.log(typeof assignee_url_html);


                                            var assignee_url = assignee_url_html.match(/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/);
                                            var assignee_url = assignee_url[1];


                                        }



                                        mentorships_tasks_text += '<div class="chek" id="chek-' + key + '"> <div> <form " action=""> <a ' + task_delete + '  class="task-delete-chk button" data-id="' + key + '" style="background:transparent;border:none;" ><i class="fa-regular fa-trash-can" style="color: #bf4040;"></i></a> <input ' + checked + ' class="task-done-chk" type="checkbox" data-id="' + key + '" name=""> <div style="">' + mentorships_tasks[key].task_name + '</div> </form> </div> <div class="imgicon"> <img class="img-thumbnail" src="' + assignee_url + '" style="border-radius: 50%; margin-right: 18px;" alt=""> <p>' + mentorships_tasks[key].task_date + '</p> </div> </div>';

                                        if (mentorships_tasks[key].task_completed == 'YES') {
                                            mentorship_task_completed += 1;
                                        }
                                        mentorships_all_tasks += 1;

                                    }

                                }
                                else {
                                    mentorships_tasks_text = '<div class="lodsec"> <div><img src="' + levup_obj.plugin_image_url + '/images/load icon.png" alt=""></div> <div class="width">Wating Mentor to assign a new tasks. <br>Send a message to remind</div> </div>'
                                }


                                var today = (new Date()).toISOString().replace(/-|:|\.\d\d\d/g, "");

                                var tomorrow = (new Date(new Date().getTime() + (24 * 60 * 60 * 1000)).toISOString().replace(/-|:|\.\d\d\d/g, ""));


                                var google_calendar_link = 'https://calendar.google.com/calendar/render?action=TEMPLATE&text=' + current_mentorship_data[0].mentorship_name + '&dates=' + today + '/' + tomorrow + '&details=Meeting with Mentor&add=' + current_mentorship_data[0].users.mentee.data.user_email + ',' + current_mentorship_data[0].users.mentor.data.user_email;

                                $('#google-calendar-link')[0].href = google_calendar_link;

                                var outlook_calendar_link = 'https://outlook.live.com/calendar/deeplink/compose?path=/calendar/action/compose&rru=addevent&startdt=' + today + '&enddt' + tomorrow + '&subject=' + current_mentorship_data[0].mentorship_name + '&body=Meeting with Mentor&online=1&to=' + current_mentorship_data[0].users.mentee.data.user_email + ',' + current_mentorship_data[0].users.mentor.data.user_email;

                                $('#outlook-calendar-link')[0].href = outlook_calendar_link;

                                $('#task-completed-count').html(mentorship_task_completed + '/' + mentorships_all_tasks);

                                var percentage = (mentorship_task_completed / mentorships_all_tasks) * 100;

                                console.log(percentage);

                                $('.progress-bar').width(percentage + '%');
                                $('.progress-bar').html(percentage + '%');

                                $('#finish-mentorship').attr('data-id', current_mentorship_data[0].mentorship_id);


                                if (percentage != 100) {
                                    $('#finish-mentorship').attr('data-toggle', 'tooltip');
                                    $('#finish-mentorship').attr('disabled', true);
                                    $('#finish-mentorship').attr('data-placement', 'top');
                                    $('#finish-mentorship').attr('title', 'Mentorship can\'t be marked as finished until all tasks are completed ');

                                }
                                else {

                                    $('#finish-mentorship').attr('disabled', false);
                                }


                                if (current_mentorship_data[0].mentor_id != current_user_id) {
                                    $('#finish-mentorship').hide();

                                    $("#levup-add-task-form").hide();

                                    $('.task-delete-chk').hide();

                                }
                                else {

                                    $("#levup-add-task-form").show();

                                }

                                $('#levup-tasks').html(mentorships_tasks_text);

                                var files_and_urls = current_mentorship_data[0].files_and_urls;

                                var files_and_urls_text = '';

                                for (const key in files_and_urls) {


                                    //  var task_delete = true ? '<a  class="task-delete-chk button" data-id="'+ key + '" style="background:transparent;border:none;" ><i class="fa-regular fa-trash-can" style="color: #bf4040;"></i></a>'  : '';

                                    files_and_urls_text += '<li><a href="' + files_and_urls[key] + '" >' + key + '</a></li>';

                                    $('#uploaded-files').html(files_and_urls_text);

                                }


                                $('.chatbox').hide();
                                $('#task-management-interface').show();


                                $('.task-delete-chk').on('click', function (e) {
                                    e.preventDefault();

                                    console.log('clicked');
                                    var parent = $("#chek-" + $(this).attr('data-id'));

                                    parent.hide();

                                    console.log(parent);


                                    var data = {
                                        action: 'levup_task_process',
                                        task_id: $(this).attr("data-id"),
                                        mentorship_id: $("#mentorship_id")[0].value,
                                        mentor_id: current_mentorship_data[0].mentor_id,
                                        mentee_id: current_mentorship_data[0].mentee_id,
                                        process: 'task_delete'
                                    };
                                    document.getElementById("overlay").style.display = "flex";

                                    $.post(levup_obj.ajax_url, data,
                                        function (response) {

                                            document.getElementById("overlay").style.display = "none";


                                            update_progress_bar(response.tasks);


                                            console.log(response);
                                        }
                                    );


                                })


                                $('.task-done-chk').on('click', function (e) {

                                    console.log('clicked');

                                    var data = {
                                        action: 'levup_task_process',
                                        task_id: $(this).attr("data-id"),
                                        mentorship_id: $("#mentorship_id")[0].value,
                                        process: 'task_complete',
                                        checked: $(this).prop('checked') == true,
                                        mentor_id: current_mentorship_data[0].mentor_id,
                                        mentee_id: current_mentorship_data[0].mentee_id,
                                    };
                                    document.getElementById("overlay").style.display = "flex";

                                    $.post(levup_obj.ajax_url, data,
                                        function (response) {

                                            document.getElementById("overlay").style.display = "none";

                                            update_progress_bar(response.tasks);

                                            console.log(response);
                                        }
                                    );


                                })


                                $('#task-files').on('change', function () {
                                    var data = new FormData();

                                    const maximumSize = 10 * 1024 * 1024

                                    if ($(this).prop('files')[0].size >= maximumSize) {
                                        $('#uploaded-files').html('File size too Large');
                                        return;
                                    }

                                    data.append('task_file', $(this).prop('files')[0]);
                                    data.append('action', 'levup_upload_mentorship_file');
                                    data.append('mentorship_id', $("#mentorship_id")[0].value);
                                    document.getElementById("overlay").style.display = "flex";
                                    data.append('mentor_id', current_mentorship_data[0].mentor_id);
                                    data.append('mentee_id', current_mentorship_data[0].mentee_id);

                                    console.log($(this).prop('files'));

                                    $.ajax({
                                        method: 'post',
                                        processData: false,
                                        contentType: false,
                                        cache: false,
                                        data: data,
                                        enctype: 'multipart/form-data',
                                        url: levup_obj.ajax_url,
                                        success: function (response) {

                                            add_files_and_urls(response.all_files);
                                            document.getElementById("overlay").style.display = "none";

                                            console.log(response);
                                        }
                                    });

                                })




                            }
                            else if (current_mentorship_data.length != 0 && current_mentorship_data[0].mentorship_started == "1") {
                                if (current_mentorship_data[0].mentor_id == current_user_id) {
                                    var type_of_mentorship_text = '';

                                    var type_of_mentorship_array = current_mentorship_data[0].type_of_mentorship;

                                    type_of_mentorship_array.forEach(element => {

                                        type_of_mentorship_text += element + ', ';

                                    });
                                    type_of_mentorship_text += 'etc.'

                                    var timeline = current_mentorship_data[0].timeline.split(' ')[0];

                                    var task_management_interface = '<div class="part31"> <div class="part311"> <img src="' + thread_user_img_src + '" class="par311-img-ment-request rounded-circle" alt="" style="width: 30%; height: 23%;"> <h3 class="text-center" style="font-size: 25px; color:black; padding: 15px 35px; font-weight: 400;"> <span style="border-bottom: 1px solid black;">' + recipient_name + '</span> wants to be your mentee. Do you want to start Mentorship? </h3> <div class="part312"><p>' + current_mentorship_data[0].intro_message + '</p> <h4>Type of Mentorship:<span> ' + type_of_mentorship_text + '</span></h4> <h4>Preferred Date:<span> ' + timeline + '</span></h4> </div> <div class="imgbox"> <button class="btn ripple-surface btn-update-mentorship-status" data-id="' + current_mentorship_data[0].mentorship_id + '"  data-action="accept"  style="background-color: rgb(139, 204, 130); color: black; box-shadow: none; border-radius: 10px; padding: 18px 45px; border-width: 1px 1px 2px; border-style: solid; border-color: black; min-width: 137.578px;"> ACCEPT <div class="ripple-wave active" style="left: -5.01802px; top: -87.018px; height: 222.036px; width: 222.036px; transition-delay: 0s, 250ms; transition-duration: 500ms, 250ms;"></div></button> <button class="btn  btn-update-mentorship-status" data-id="' + current_mentorship_data[0].mentorship_id + '"  data-action="reject"" style="background-color: white; color: black; box-shadow: none; border-radius: 10px; padding: 18px 45px; border-width: 1px 1px 2px; border-style: solid; border-color: black;"> REJECT </button> </div> </div> </div>';

                                    $('#mentorship-accept-reject').html(task_management_interface);
                                    $('.chatbox').hide();

                                    $('#mentorship-accept-reject').show();

                                }
                                if (current_mentorship_data[0].mentee_id == current_user_id) {

                                    var task_management_interface = '<div class="chatbox2"> <div class="part31"> <div class="part311"> <div><svg width="408" height="384" viewBox="0 0 408 384" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M230.417 205.67C227.86 204.806 225.322 203.885 222.784 202.944C214.42 199.853 206.23 196.57 199.039 191.232H167.756C167.756 191.27 167.775 191.29 167.775 191.328C168.891 202.982 168.756 213.773 166.103 224.755C165.084 231.821 163.449 238.79 161.046 245.76L162.892 258.49H235.493L230.417 205.67Z" fill="#FFD13A"></path> <path d="M179.811 180.902C181.907 178.214 184.484 175.661 186.502 172.8C184.56 166.118 184.234 157.862 186.156 147.283C153.528 147.917 120.88 148.205 88.2324 148.109C100.903 158.65 117.573 165.926 143.568 172.55C154.585 175.354 164.795 181.843 168.14 193.632C172.87 190.176 176.235 185.53 179.811 180.902Z" fill="#D9D9D9"></path> <path d="M255.451 274.31C256.355 273.274 256.97 271.968 257.085 270.374C256.932 269.914 256.759 269.472 256.566 269.011C255.067 268.57 253.24 268.608 252.163 268.858C250.125 269.338 248.279 270.317 246.549 271.43C245.761 271.949 244.607 271.738 243.953 271.104L243.761 270.912C243.338 270.49 243.107 269.875 243.146 269.261C232.859 270.01 222.554 270.739 212.267 271.392C194.232 272.563 176.158 272.525 158.143 273.869C158.354 303.034 171.678 328.55 171.678 328.55L243.703 293.914C242.934 293.818 242.165 293.683 241.415 293.51C240.223 293.51 239.031 293.453 237.935 293.203C236.532 292.877 235.224 291.936 235.282 290.362C235.32 289.363 235.916 288.518 236.782 288.23L236.916 287.386C236.801 287.27 236.685 287.155 236.589 287.002C236.013 286.56 235.647 285.888 235.57 285.101C235.474 283.872 236.262 282.989 237.416 282.835C232.571 274.445 249.625 276.326 255.451 274.31Z" fill="#D9D9D9"></path> <path d="M189.464 246.144C189.464 247.046 189.137 247.795 188.618 248.333C188.33 248.621 188.003 248.832 187.618 248.986C187.253 249.12 186.869 249.216 186.446 249.216C186.426 249.216 186.407 249.216 186.369 249.216C185.1 249.216 184.004 248.448 183.542 247.334C183.388 246.97 183.312 246.566 183.312 246.144C183.312 245.549 183.446 244.992 183.735 244.512C183.811 244.397 183.888 244.262 183.965 244.166L184.061 244.032C184.6 243.437 185.388 243.072 186.369 243.072C188.099 243.091 189.464 244.454 189.464 246.144Z" fill="#FFD13A"></path> <path d="M189.675 208.954C188.964 213.293 188.252 217.632 187.541 221.952C187.368 223.027 189.021 223.507 189.214 222.413C189.925 218.074 190.637 213.734 191.348 209.414C191.521 208.32 189.848 207.859 189.675 208.954Z" fill="#FF3857"></path> <path d="M186.33 202.291C185.811 205.114 185.292 207.936 184.792 210.758C184.6 211.834 186.253 212.314 186.465 211.219C186.984 208.397 187.503 205.574 188.003 202.752C188.195 201.677 186.542 201.216 186.33 202.291Z" fill="#FF3857"></path> <path d="M101.461 229.363C96.1353 225.869 90.8093 222.394 85.4834 218.899C84.5605 218.285 83.6953 219.782 84.6182 220.378C89.9441 223.872 95.27 227.347 100.596 230.842C101.519 231.456 102.384 229.958 101.461 229.363Z" fill="#7BAEED"></path> <path d="M103.98 184.666L99.75 191.597L107.941 193.67L111.113 186.835L103.98 184.666Z" fill="#FF3857"></path> <path d="M269.102 60.7872C267.987 63.456 266.853 66.1056 265.738 68.7744C265.315 69.792 266.968 70.2336 267.41 69.2352C268.525 66.5664 269.66 63.9168 270.775 61.248C271.198 60.2304 269.525 59.7696 269.102 60.7872Z" fill="#FFD13A"></path> <path d="M144.876 138.106L115.305 141.178L111.113 153.293L148.722 151.2L144.876 138.106Z" fill="#FFD13A"></path> <path d="M172.371 172.166L170.736 176.275L177.524 177.773L178.235 172.166H172.371Z" fill="#FFD13A"></path> <path d="M177.523 303.802V315.744L190.656 311.04L177.523 303.802Z" fill="#FFD13A"></path> <path d="M207.192 235.2C205.481 233.28 203.75 231.36 202.039 229.459C201.289 228.634 200.078 229.862 200.828 230.669C202.539 232.589 204.269 234.509 205.981 236.41C206.73 237.235 207.942 236.026 207.192 235.2Z" fill="white"></path> <path d="M206.115 228.691C204.211 231.322 202.289 233.952 200.385 236.582C199.731 237.485 201.231 238.349 201.866 237.446C203.769 234.816 205.692 232.186 207.595 229.555C208.249 228.672 206.769 227.808 206.115 228.691Z" fill="white"></path> <path d="M171.66 329.453C171.353 329.453 171.045 329.28 170.891 328.992C170.699 328.627 151.452 291.226 159.028 255.475C159.759 252.019 160.47 248.698 161.162 245.51C170.91 200.218 174.929 181.459 143.358 173.414C101.577 162.758 82.7731 150.374 66.0455 122.438C65.9301 122.227 65.8917 121.997 65.9494 121.766C66.007 121.536 66.1609 121.344 66.3724 121.229L177.121 62.0736C177.332 61.9584 177.563 61.9392 177.774 62.016C177.986 62.0928 178.178 62.2272 178.294 62.4384C178.486 62.8032 197.617 99.936 187.907 143.136C178.813 183.61 200.02 192.576 224.573 202.963C231.418 205.862 238.513 208.858 245.319 212.659C260.874 221.357 274.083 231.648 284.62 243.264C284.947 243.61 284.908 244.166 284.562 244.474C284.216 244.781 283.658 244.762 283.351 244.416C272.949 232.934 259.874 222.758 244.493 214.157C237.763 210.394 230.726 207.418 223.92 204.538C198.655 193.862 176.832 184.627 186.234 142.752C195.021 103.622 179.832 69.5424 177.159 64.0128L67.9874 122.304C84.3305 149.242 102.904 161.299 143.781 171.725C176.967 180.192 172.545 200.774 162.835 245.856C162.143 249.043 161.431 252.365 160.701 255.821C153.856 288.192 169.391 322.022 172.025 327.437L243.339 293.146C243.762 292.934 244.281 293.126 244.493 293.549C244.704 293.971 244.512 294.49 244.089 294.701L172.006 329.357C171.91 329.414 171.795 329.453 171.66 329.453Z" fill="#000200"></path> <path d="M66.7957 121.958L63.5078 115.162L174.602 58.4064L177.525 62.8032L66.7957 121.958Z" fill="#000200"></path> <path d="M66.7953 122.822C66.6992 122.822 66.6223 122.803 66.5261 122.784C66.2954 122.707 66.1224 122.554 66.007 122.342L62.7192 115.546C62.5077 115.123 62.6807 114.624 63.1037 114.413L174.198 57.6384C174.583 57.4464 175.063 57.5616 175.313 57.9264L178.236 62.3232C178.37 62.5344 178.409 62.784 178.351 63.0144C178.294 63.2448 178.14 63.456 177.928 63.5712L67.1799 122.726C67.0645 122.784 66.9299 122.822 66.7953 122.822ZM64.6419 115.546L67.1606 120.787L176.275 62.496L174.294 59.5392L64.6419 115.546Z" fill="#000200"></path> <path d="M240.801 295.354L171.66 328.589L175.313 336.154L253.606 298.522C249.146 298.387 244.8 297.542 240.801 295.354Z" fill="#000200"></path> <path d="M175.295 336.998C175.198 336.998 175.102 336.979 175.006 336.941C174.795 336.864 174.622 336.71 174.506 336.499L170.853 328.934C170.642 328.512 170.834 327.994 171.257 327.782L240.398 294.547C240.648 294.432 240.936 294.432 241.186 294.566C244.724 296.506 248.781 297.504 253.588 297.619C253.991 297.638 254.318 297.907 254.414 298.291C254.491 298.675 254.299 299.078 253.953 299.251L175.66 336.883C175.564 336.979 175.429 336.998 175.295 336.998ZM172.814 328.992L175.718 335.002L250.3 299.155C246.762 298.752 243.628 297.811 240.801 296.333L172.814 328.992Z" fill="#000200"></path> <path d="M183.638 255.936C181.504 255.936 179.37 255.744 177.274 255.322C171.718 254.227 167.699 251.654 165.353 247.68C161.162 240.595 161.873 229.286 167.68 211.027C167.814 210.566 168.314 210.336 168.757 210.47C169.218 210.624 169.468 211.104 169.314 211.546C163.738 229.075 162.95 240.269 166.815 246.797C168.93 250.368 172.467 252.614 177.582 253.613C194.175 256.877 211.998 245.472 215.344 238.061C216.651 235.142 216.94 232.205 217.228 229.363C217.709 224.371 218.19 219.667 224.188 216.672C228.764 214.387 231.995 214.042 233.091 215.712C233.956 217.056 233.052 219.014 230.687 220.973C228.149 223.066 226.534 225.773 226.38 228.25C226.303 229.363 226.553 230.342 227.072 230.899C227.149 230.918 227.572 230.938 228.476 230.458C231.918 228.614 237.109 222.701 237.532 219.398C237.744 217.67 237.494 216.134 237.301 214.925C237.051 213.446 236.859 212.275 237.57 211.373C238.205 210.605 239.243 210.451 239.993 210.413C241.262 210.336 242.397 210.778 243.281 211.68C245.281 213.715 245.569 217.594 245.434 219.552C245.223 222.528 241.262 230.131 240.801 230.995C240.57 231.418 240.051 231.571 239.628 231.36C239.205 231.149 239.051 230.611 239.262 230.189C240.435 227.942 243.531 221.645 243.704 219.418C243.839 217.594 243.492 214.349 242.031 212.87C241.493 212.314 240.839 212.083 240.07 212.122C239.224 212.16 238.974 212.352 238.916 212.429C238.686 212.717 238.839 213.677 238.993 214.618C239.205 215.942 239.493 217.613 239.243 219.59C238.666 224.141 231.61 231.341 228.188 232.435C226.899 232.858 226.169 232.454 225.803 232.051C224.957 231.13 224.534 229.747 224.65 228.115C224.842 225.178 226.688 222.01 229.591 219.629C231.437 218.112 231.841 216.922 231.649 216.634C231.379 216.211 229.303 216.019 224.957 218.189C219.805 220.762 219.42 224.621 218.939 229.517C218.651 232.358 218.343 235.584 216.921 238.752C213.479 246.509 198.616 255.936 183.638 255.936Z" fill="#000200"></path> <path d="M245.319 236.064C245.166 236.064 244.993 236.026 244.858 235.93C244.454 235.68 244.339 235.142 244.589 234.739C244.627 234.682 247.992 229.402 248.857 224.736C249.722 220.09 249.203 217.306 243.916 215.981C243.454 215.866 243.166 215.405 243.281 214.925C243.397 214.464 243.858 214.176 244.339 214.31C251.261 216.058 251.414 220.512 250.568 225.062C249.626 230.074 246.204 235.45 246.05 235.68C245.877 235.93 245.608 236.064 245.319 236.064Z" fill="#000200"></path> <path d="M250.068 240.365C249.914 240.365 249.741 240.326 249.607 240.23C249.203 239.981 249.088 239.443 249.338 239.04C249.357 239.021 250.972 236.429 251.952 232.166C253.048 227.309 252.664 221.165 250.51 220.512C250.049 220.378 249.799 219.898 249.934 219.437C250.068 218.976 250.549 218.726 251.01 218.861C255.086 220.07 254.548 228.595 253.644 232.531C252.606 237.12 250.876 239.846 250.818 239.942C250.626 240.211 250.357 240.365 250.068 240.365Z" fill="#000200"></path> <path d="M128.995 255.936C128.899 255.936 128.803 255.917 128.706 255.878C128.264 255.706 128.033 255.226 128.187 254.765C131.264 246.374 145.299 232.205 163.796 237.466C164.257 237.6 164.527 238.08 164.392 238.522C164.257 238.982 163.777 239.251 163.334 239.117C145.549 234.086 132.321 248.486 129.802 255.341C129.668 255.725 129.341 255.936 128.995 255.936Z" fill="#000200"></path> <path d="M183.889 296.314C178.986 296.314 174.102 295.718 169.276 294.528C151.818 290.189 137.321 278.131 130.514 262.253C117.113 230.995 123.419 182.65 185.311 149.28C185.734 149.05 186.254 149.203 186.484 149.626C186.715 150.048 186.561 150.566 186.138 150.797C125.284 183.61 118.997 230.976 132.11 261.581C138.705 276.96 152.76 288.653 169.699 292.858C188.503 297.523 208.672 292.646 225.054 279.456C259.028 252.096 256.01 233.011 253.337 229.402C253.049 229.018 253.145 228.48 253.51 228.192C253.895 227.904 254.433 227.981 254.721 228.365C258.259 233.146 260.586 253.056 226.131 280.8C213.498 290.957 198.674 296.314 183.889 296.314Z" fill="#000200"></path> <path d="M183.639 111.782C185.369 107.328 181.966 98.4192 184.042 92.7936C185.792 88.032 190.291 85.6896 194.925 84.3648C201.77 82.3872 209.057 82.5024 216.017 80.9472C217.459 80.6208 219.863 79.8144 222.381 79.2192C218.594 74.6688 211.749 71.8272 199.424 73.2672C178.851 75.6672 170.968 81.696 172.564 96.9984C173.891 109.651 176.217 112.416 176.217 112.416L176.429 112.685C179.37 111.955 183.389 112.397 183.639 111.782Z" fill="#000200"></path> <path d="M214.075 119.424C213.652 119.424 213.268 119.098 213.21 118.656C213.152 118.176 213.499 117.754 213.96 117.696C215.556 117.504 216.825 116.928 217.363 116.141C217.652 115.699 217.728 115.181 217.555 114.586C217.171 113.261 214.037 112.877 212.633 112.877C212.191 112.877 211.826 112.55 211.768 112.109L210.845 104.275C210.787 103.795 211.134 103.373 211.595 103.315C212.076 103.277 212.499 103.603 212.556 104.064L213.383 111.168C215.017 111.264 218.517 111.744 219.209 114.106C219.517 115.181 219.382 116.218 218.805 117.082C217.978 118.33 216.325 119.155 214.171 119.405C214.133 119.424 214.095 119.424 214.075 119.424Z" fill="#000200"></path> <path d="M214.94 102.336L215.132 104.045C215.382 106.253 218.824 105.85 218.554 103.642L218.362 101.933C218.112 99.7248 214.69 100.128 214.94 102.336Z" fill="#000200"></path> <path d="M194.31 104.736L194.502 106.445C194.752 108.653 198.194 108.25 197.925 106.042L197.732 104.333C197.482 102.125 194.06 102.528 194.31 104.736Z" fill="#000200"></path> <path d="M198.79 96.6912C197.367 96.7104 195.982 96.8256 194.598 97.1328C193.521 97.3632 194.098 98.9952 195.175 98.7648C196.405 98.496 197.674 98.4384 198.924 98.4192C200.02 98.4 199.885 96.672 198.79 96.6912Z" fill="#000200"></path> <path d="M221.227 89.8944C220.285 89.4144 219.266 89.088 218.363 88.5312C217.42 87.9552 216.344 89.2992 217.286 89.8752C218.324 90.5088 219.459 90.912 220.535 91.4496C221.535 91.9872 222.208 90.4128 221.227 89.8944Z" fill="#000200"></path> <path d="M228.61 97.9968C228.245 95.3472 227.861 92.5824 227.495 89.7216C225.707 76.2432 216.228 70.4256 199.347 72.384C180.062 74.6304 171.929 83.0208 173.775 98.784L175.371 112.474C175.428 112.954 175.851 113.28 176.332 113.222C176.813 113.165 177.139 112.742 177.082 112.262L175.486 98.5728C173.755 83.7696 181.177 76.224 199.539 74.0736C215.517 72.2112 224.111 77.3952 225.784 89.9136C226.169 92.7744 226.553 95.5392 226.899 98.1888C230.322 123.226 232.398 138.355 224.669 144.096C218.036 149.03 205.115 149.741 204.98 149.76C204.692 149.779 204.423 149.933 204.269 150.182C204.115 150.432 204.115 150.739 204.25 151.008C207.115 156.557 217.843 157.613 218.286 157.651C218.305 157.651 218.343 157.651 218.362 157.651C218.593 157.651 218.824 157.555 218.978 157.402C219.151 157.21 219.247 156.96 219.208 156.691L218.324 149.126C218.324 149.107 218.324 149.069 218.305 149.05C220.958 148.186 223.573 147.034 225.669 145.478C234.244 139.162 232.129 123.667 228.61 97.9968Z" fill="#000200"></path> <path d="M185.715 150.893C185.311 150.893 184.946 150.605 184.869 150.202L179.87 125.338C176.044 125.76 173.737 124.742 172.468 123.782C171.025 122.688 170.122 121.133 169.93 119.386C169.795 118.214 169.91 116.486 171.275 114.854C173.217 112.531 177.082 111.149 182.735 110.784C183.235 110.746 183.619 111.11 183.658 111.59C183.696 112.07 183.331 112.474 182.85 112.512C177.755 112.838 174.217 114.029 172.602 115.968C171.833 116.89 171.506 117.984 171.641 119.194C171.737 119.923 172.083 121.325 173.506 122.4C175.082 123.59 177.486 123.994 180.428 123.514C180.889 123.456 181.312 123.744 181.408 124.186L186.561 149.837C186.657 150.298 186.35 150.758 185.888 150.854C185.83 150.893 185.773 150.893 185.715 150.893Z" fill="#000200"></path> <path d="M224.131 71.136C222.074 69.5616 218.998 69.9648 216.613 70.1568C211.441 70.56 206.308 71.3856 201.27 72.576C198.905 73.1328 195.56 73.5168 194.848 76.3392C194.329 78.4128 195.637 80.1216 197.06 81.4848C196.54 81.3312 196.002 81.1776 195.483 81.0048C194.445 80.6592 193.733 82.2336 194.791 82.5792C202.52 85.1328 210.826 85.2672 218.498 82.3488C222.382 80.8896 229.688 75.36 224.131 71.136Z" fill="#000200"></path> <path d="M224.169 121.21C223.477 122.246 222.785 123.302 222.073 124.339C221.458 125.261 222.958 126.125 223.554 125.203C224.246 124.166 224.938 123.11 225.649 122.074C226.284 121.152 224.784 120.288 224.169 121.21Z" fill="#000200"></path> <path d="M206.076 170.477C199.193 170.477 190.733 166.387 184.984 157.286C184.734 156.883 184.849 156.346 185.253 156.096C185.657 155.846 185.561 155.021 185.811 155.424C185.945 155.654 186.715 156.806 186.868 157.037C193.444 166.944 203.365 170.189 209.902 168.25C214.825 166.79 217.593 162.624 217.497 156.864C217.497 156.384 217.862 156 218.343 155.981C218.785 155.942 219.208 156.346 219.228 156.826C219.343 163.334 216.036 168.211 210.402 169.901C209.076 170.285 207.614 170.477 206.076 170.477Z" fill="#000200"></path> <path d="M259.432 262.867C258.97 262.867 258.586 262.483 258.567 262.022L257.471 192.845C257.471 192.365 257.836 191.981 258.317 191.962C258.317 191.962 258.317 191.962 258.336 191.962C258.797 191.962 259.182 192.346 259.201 192.806L260.297 261.984C260.297 262.464 259.913 262.867 259.432 262.867Z" fill="#000200"></path> <path d="M255.991 298.368C248.569 298.368 243.204 296.966 241.186 294.49C240.878 294.125 240.936 293.568 241.32 293.28C241.686 292.973 242.243 293.03 242.532 293.414C244.224 295.507 249.453 296.717 256.51 296.659C273.526 296.506 296.31 289.229 306.942 275.597C321.901 256.397 323.862 214.579 304.135 187.373C284.754 160.666 237.09 148.109 217.594 150.163C217.113 150.221 216.69 149.875 216.652 149.395C216.594 148.915 216.94 148.493 217.421 148.454C237.283 146.362 285.793 159.149 305.539 186.374C325.727 214.195 323.651 256.992 308.327 276.672C296.867 291.379 273.449 298.253 256.548 298.406C256.356 298.368 256.183 298.368 255.991 298.368Z" fill="#000200"></path> <path d="M242.666 283.718C240.032 283.718 237.706 283.411 236.36 282.509C236.245 282.432 236.148 282.336 236.091 282.221C236.052 282.144 235.052 280.32 235.898 278.534C236.571 277.133 238.129 276.23 240.551 275.827C241.859 275.616 243.32 275.424 244.839 275.232C249.627 274.637 256.183 273.83 256.51 271.488C256.625 270.586 256.356 269.952 255.683 269.568C254.376 268.838 252.068 269.222 250.626 270.413C247.646 272.851 243.897 273.427 242.263 271.699C241.667 271.066 240.59 269.261 243.512 266.074C248.204 260.947 251.78 260.678 260.759 261.197C264.778 261.427 270.873 260.006 276.006 255.283C281.505 250.234 284.505 242.573 284.735 233.146C285.139 215.75 276.333 203.635 258.606 197.126C258.163 196.954 257.933 196.474 258.087 196.013C258.26 195.571 258.74 195.341 259.202 195.494C277.448 202.195 286.87 215.213 286.466 233.165C286.177 245.683 281.255 252.768 277.179 256.531C271.642 261.619 265.008 263.174 260.663 262.906C252.242 262.406 249.127 262.483 244.781 267.226C243.474 268.646 242.974 269.952 243.512 270.509C244.262 271.315 246.993 271.142 249.531 269.069C251.53 267.418 254.606 266.976 256.529 268.051C257.837 268.781 258.452 270.106 258.24 271.718C257.74 275.366 251.299 276.154 245.07 276.922C243.57 277.114 242.128 277.286 240.84 277.498C239.052 277.805 237.879 278.4 237.475 279.245C237.11 279.994 237.379 280.819 237.533 281.165C241.051 283.123 253.068 281.011 257.51 280.032C257.971 279.917 258.433 280.224 258.548 280.685C258.644 281.146 258.356 281.606 257.894 281.722C256.664 282.01 248.8 283.718 242.666 283.718Z" fill="#000200"></path> <path d="M244.781 289.037C242.128 289.037 239.475 288.768 237.129 288C234.937 287.29 234.168 285.466 234.379 284.083C234.629 282.528 235.975 281.51 237.821 281.51C238.302 281.51 238.686 281.894 238.686 282.374C238.686 282.854 238.302 283.238 237.821 283.238C236.84 283.238 236.206 283.661 236.091 284.352C235.975 285.024 236.437 285.946 237.667 286.349C245.512 288.902 257.971 285.466 258.106 285.446C258.567 285.331 259.048 285.581 259.163 286.042C259.298 286.502 259.029 286.982 258.567 287.098C258.183 287.213 251.568 289.037 244.781 289.037Z" fill="#000200"></path> <path d="M246.108 294.797C242.974 294.797 239.782 294.49 237.129 293.626C234.937 292.915 234.168 291.091 234.379 289.709C234.629 288.154 235.975 287.136 237.821 287.136C238.302 287.136 238.686 287.52 238.686 288C238.686 288.48 238.302 288.864 237.821 288.864C236.84 288.864 236.206 289.286 236.091 289.978C235.975 290.65 236.437 291.571 237.667 291.974C245.55 294.547 259.125 291.686 259.259 291.648C259.721 291.552 260.182 291.84 260.278 292.32C260.374 292.781 260.086 293.242 259.625 293.338C259.221 293.453 252.799 294.797 246.108 294.797Z" fill="#000200"></path> <path d="M276.852 208.512C276.814 208.512 276.775 208.512 276.737 208.512C276.256 208.454 275.929 208.013 276.006 207.552C277.141 199.219 280.659 192.672 286.177 188.64C291.138 185.011 297.733 183.456 303.809 184.474C304.27 184.55 304.597 184.992 304.52 185.472C304.443 185.933 304.001 186.259 303.52 186.182C297.887 185.222 291.792 186.662 287.197 190.042C281.986 193.862 278.813 199.834 277.718 207.802C277.641 208.205 277.275 208.512 276.852 208.512Z" fill="#000200"></path> <path d="M265.738 199.718C265.738 199.718 261.009 191.981 258.336 191.981V195.994L265.738 199.718Z" fill="#000200"></path> <path d="M274.448 132.269C263.623 132.269 253.914 125.818 249.722 115.834C248.338 112.55 247.646 109.075 247.646 105.504C247.646 100.339 248.895 95.4432 251.279 91.3152C251.856 90.2784 252.568 89.2416 253.356 88.2432L254.26 87.168C259.239 81.6192 266.2 78.7392 274.448 78.7392C289.234 78.7392 301.251 90.7584 301.251 105.504C301.251 113.05 298.694 119.616 293.868 124.512C291.445 126.989 288.522 128.909 285.177 130.253C282.024 131.52 278.62 132.192 275.044 132.269H274.448ZM274.448 80.448C266.7 80.448 260.182 83.136 255.586 88.2432L254.683 89.3184C253.952 90.2208 253.318 91.1808 252.76 92.16C250.53 96.0384 249.357 100.646 249.357 105.504C249.357 108.864 250.01 112.109 251.299 115.181C255.221 124.512 264.315 130.56 274.448 130.56H274.986C278.37 130.483 281.562 129.85 284.523 128.678C287.638 127.43 290.368 125.626 292.618 123.322C297.117 118.752 299.501 112.608 299.501 105.523C299.54 91.6992 288.292 80.448 274.448 80.448Z" fill="#000200"></path> <path d="M283.965 112.243H274.102C273.64 112.243 273.275 111.898 273.237 111.437L272.333 96.7488C272.294 96.2688 272.66 95.8656 273.14 95.8464C273.64 95.808 274.025 96.1728 274.063 96.6528L274.909 110.515H283.965C284.446 110.515 284.831 110.899 284.831 111.379C284.831 111.859 284.446 112.243 283.965 112.243Z" fill="#000200"></path> </svg> </div> <div class="text1"> Your request has been sent.<br>And We/\'re waiting him to response</div> <div style="font-size: 20px; margin-top: 15px"><button class="btn btn-warning btn-update-mentorship-status"  data-id="' + current_mentorship_data[0].mentorship_id + '"  data-action="reject">Cancel Request</button></div> </div> </div> </div>';

                                    $('#mentorship-accept-reject').html(task_management_interface);
                                    $('.chatbox').hide();

                                    $('#mentorship-accept-reject').show();

                                }

                            }
                            else {
                                console.log(current_mentorship_data.length, current_mentorship_data.mentorship_started);

                                var recipient = $(this).attr("data-recipient-name");

                                var task_management_interface = '<div class="part31"> <div><img src="' + thread_user_img_src + '" style="width:60px;height:auto;"></div> <div class="text"> Would you like to <br>be <u>' + recipient_name + '/\'s</u> mentee?</div> <div class="text1">You will unlock more structured way <br> to complete your goals.</div> <div> <a class="btn btn-success" href="' + levup_obj.site_url + '/request-a-mentorship/?mentor=' + recipient_id + '"  >SEND A REQUEST</a> </div> </div>'
                                $('#levup-send-request').html(task_management_interface);
                                $('.chatbox').hide();
                                ``
                                $('#levup-send-request').show();


                            }
                            $('.btn-update-mentorship-status').on('click', function () {
                                var data = {
                                    action: 'levup_update_mentorship_status',
                                    mentorship_action: $(this).attr('data-action'),
                                    mentorship_id: $(this).attr('data-id'),
                                    mentorship_data: current_mentorship_data[0]
                                }

                                $.post(levup_obj.ajax_url, data,
                                    function (response) {

                                        if (response.status == 2) {
                                            document.location.reload();
                                        }

                                    }
                                );

                            })


                            var read_messages_data = {
                                id: thread_id,
                                message_id: messages[messages.length - 1],
                                action: 'messages_thread_read',
                                nonce: $('#user_threads_nonce')[0].value
                            }

                            $.post(levup_obj.ajax_url, read_messages_data, function (response) {

                            })


                        }
                    );


                    console.log('clicked');

                })

                if (window.location.pathname == '/chatbox/') {
                    if (urlParams.has('thread') && urlParams.get('thread') != null && $('[data-thread-id="' + urlParams.get('thread') + '"]').length != 0) {
                        var thread_link = $('[data-thread-id="' + urlParams.get('thread') + '"]');

                        if (thread_link.length != 0) {
                            thread_link.click();
                            thread_link[0].click();
                        }
                        console.log(urlParams.get('thread'));
                        console.log(thread_link);
                    }
                    else {
                        var thread_link = $('[data-thread-id="' + threads[0].id + '"]');

                        console.log(thread_link);
                        if (thread_link.length != 0) {
                            thread_link.click();
                            thread_link[0].click();
                        }

                    }
                }



            },
        );

        $('#levup-send-reply').on('submit', function (e) {
            e.preventDefault();
            const date = new Date()

            const utcStr = date.toUTCString()

            var data = {
                id: $('#message').attr("data-thread-id"),
                nonce: $('#levup_message_nonce')[0].value,
                action: "messages_send_reply",
                hash: new Date().getTime(),
                send_at: getUTCDateTime,
                id: 0,
                content: $('#message')[0].value,
                sender_id: 0,
                sender_name: "",
                sender_link: "",
                is_user_blocked: false,
                is_user_suspended: false,
                sender_avatar: "",
                date: 0,
                display_date: "",
                is_group: false,
                thread_id: $('#message').attr("data-thread-id"),
                sending: true,
            }


            if (levup_attachments.items.length != 0) {
                var attach_param = '';
                if (levup_attachments.type == 'image') {
                    attach_param = 'media';
                }
                else if (levup_attachments.type == 'video') {
                    attach_param = 'video';
                }
                else if (levup_attachments.type == 'document') {
                    attach_param = 'document';
                }

                data[attach_param] = [];
                levup_attachments.items.forEach(element => {
                    element.privacy = 'message';
                    element.saved = false;
                    data[attach_param].push(element);
                });

            }



            //         console.log(data);

            $.post(levup_obj.ajax_url, data,
                function (response) {

                    if (response.success == true) {

                        window.Backbone.trigger(
                            'relistelements',
                            {
                                hash: response.data.hash,
                                message: response.data.messages[0],
                                recipient_inbox_unread_counts: response.data.recipient_inbox_unread_counts,
                                thread_id: response.data.thread_id
                            }
                        );
                        var threads_text = '';

                        //     console.log(response);

                        var messages = response.data.messages;
                        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch

                        messages.reverse();

                        messages.forEach(message => {
                            threads_text += '<li class=""> <div class="bp-single-message-wrap"> <div class="bp-avatar-wrap"> <a href="' + message.sender_link + '" class="bp-user-avatar"> <img class="avatar" src="' + message.sender_avatar + '" alt=""> </a> </div> <div class="bp-single-message-content"> <div class="message-metadata"> <a href="' + message.sender_link + '" class="bp-user-link"> <strong>' + message.sender_name + '</strong> </a> <time datetime="' + d.setUTCSeconds(message.date) + '" class="activity">' + message.display_date + '</time> </div> <div class="bp-message-content-wrap"><p>' + message.content + '</p> </div>';

                            if (message.media != undefined) {
                                threads_text += '<div class="bb-activity-media-wrap bb-media-length-1">';
                                message.media.forEach(element => {

                                    threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="' + element.privacy + '" href="#"> <img  src="' + element.thumbnail + '" alt="' + element.thumbnail + '"> </a> </div> '

                                });
                                threads_text += '</div>';
                            }
                            if (message.video != undefined) {
                                console.log(message.video_html)

                                threads_text += '<div class="bb-activity-video-wrap bb-video-length-1 embed-responsive" >'

                                message.video.forEach(element => {

                                    //   threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="'+ element.privacy +'" href="#"> <img  src="' + element.thumbnail + '" alt="'+ element.thumbnail +'"> </a> </div> ';

                                    element.video_html = element.video_html.replace('<video', '<video style="width:100%;height:100%; "');

                                    threads_text += element.video_html;

                                });

                                threads_text += '</div>'
                            }
                            if (message.document != undefined) {
                                console.log(message.document)

                                threads_text += '<div style="width:90%;" class="bb-activity-media-wrap bb-media-length-1 embed-responsive" >'

                                message.document.forEach(element => {
                                    console.log(element)

                                    threads_text += '<div style="width:90%;"  class="bb-activity-media-elem document-activity " data-id="' + element.id + ' "> <div class="document-description-wrap"> <a href="' + element.url + ' " class="entry-img" data-id="4" data-activity-id="4"> <i class="bb-icon-file-pdf"></i> </a> <a href="' + element.url + ' " class="document-detail-wrap bb-open-document-theatre" data-id="' + element.id + ' " data-activity-id="" data-icon-class="bb-icon-file-pdf&quot;" data-attachment-id="' + element.attachment_id + ' " data-attachment-full="" data-privacy="message" data-extension="' + element.extension + ' " data-author="' + element.author + ' " data-preview="" data-full-preview="" data-text-preview="' + element.text_preview + ' " data-mp3-preview="" data-document-title="' + element.document_title + ' " data-video-preview="" data-mirror-text=""> <span class="document-title">' + element.document_title + ' </span> <span class="document-description">' + element.size + ' </span> <span class="document-extension-description">' + element.extension + ' </span> <span class="document-helper-text"> <span> - </span> <span class="document-helper-text-inner">Click to view</span></span> </a> </div> <div class="document-action-wrap"> <a href="#" class="document-action_collapse" data-balloon-pos="up" data-tooltip-collapse="Collapse" data-balloon="Expand"><i class="bb-icon-merge bb-icon-l document-icon-collapse"></i></a> <a href="' + element.url + ' " class="document-action_download" data-balloon-pos="up" data-balloon="Download"> <i class="bb-icon-l bb-icon-download"></i> </a> <a href="#" target="_blank" class="document-action_more" data-balloon-pos="up" data-balloon="More actions"> <i class="bb-icon-f bb-icon-ellipsis-h"></i> </a> <div class="document-action_list"> <ul> <li class="copy_download_file_url"> <a href="' + element.url + ' ">Copy Download Link</a> </li> </ul> </div> </div> </div>';

                                });

                                threads_text += '</div>'
                            }


                            threads_text += '</div> </div> </li>';
                        });




                        $('#levup-message-thread-list')[0].innerHTML += threads_text;

                        levup_attachments.items = [];
                        levup_attachments.type = '';

                        $('#levup-send-reply').trigger("reset");
                        $('.messages-media-container').hide();



                    }

                    console.log(response);
                }
            );


        });

    }
    function jQFormSerializeArrToJson(formSerializeArr) {
        var jsonObj = {};
        jQuery.map(formSerializeArr, function (n, i) {
            jsonObj[n.name] = n.value;
        });

        return jsonObj;
    }

    $('#levup-add-task-form').on('submit', function (e) {
        e.preventDefault();
        var data = $('#levup-add-task-form').serialize();

        var data_array = $('#levup-add-task-form').serializeArray();
        var data_array = jQFormSerializeArrToJson(data_array);
        /*
                     if(data_array.task_assigned_to == 'mentee')
                     {
        
                        if(current_mentorship_data[0].mentee_id == current_user_id ){
        
                        }
                        
        
                        data_array.asignee_url = '';
        
                     }
        */


        $.post(levup_obj.ajax_url, data,
            function (response) {

                var last_task = '';
                var key = response.task_added.key;

                //   $('#levup-tasks')[0].innerHTML += '<div class="chek"> <div> <form action=""> <input class="task-done-chk" type="checkbox" data-id="'+ key + ' name=""> <div style="">'+ data_array.task_name +'</div> </form> </div> <div class="imgicon"> <img class="img-thumbnail" src="assert/images/My project.jpg" style="border-radius: 50%; margin-right: 18px;" alt=""> <img src="assert/images/icons8-calendar-64.png" height="15px" width="15px" alt="" class="mgdate"> <p>'+ data_array.task_date +'</p> </div> </div>'

                var mentorships_tasks_text = '';

                var mentorships_tasks = response.all_tasks;

                update_progress_bar(response.all_tasks);


                var mentorship_task_completed = 0;
                var mentorships_all_tasks = 0;

                for (const key in mentorships_tasks) {

                    var checked = mentorships_tasks[key].task_completed == 'YES' ? 'checked' : '';

                    var assignee_url_html = mentorships_tasks[key].assignee_url;

                    //   var assignee_url_match = assignee_url_html.match(/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/)

                    var assignee_url = '';

                    if (assignee_url_html != null) {
                        console.log(typeof assignee_url_html);


                        var assignee_url = assignee_url_html.match(/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/);
                        var assignee_url = assignee_url[1];


                    }



                    var task_delete = true ? '<a  class="task-delete-chk button" data-id="' + key + '" style="background:transparent;border:none;" ><i class="fa-regular fa-trash-can" style="color: #bf4040;"></i></a>' : '';

                    mentorships_tasks_text += '<div class="chek" id="chek-' + key + '"> <div> <form " action=""> ' + task_delete + ' <input ' + checked + ' class="task-done-chk" type="checkbox" data-id="' + key + '" name=""> <div style="">' + mentorships_tasks[key].task_name + '</div> </form> </div> <div class="imgicon"> <img class="img-thumbnail" src="' + assignee_url + '" style="border-radius: 50%; margin-right: 18px;" alt=""> <img src="assert/images/icons8-calendar-64.png" height="15px" width="15px" alt="" class="mgdate"/> <p>' + mentorships_tasks[key].task_date + '</p> </div> </div>';
                    if (mentorships_tasks[key].task_completed == 'YES') {
                        mentorship_task_completed += 1;
                    }
                    mentorships_all_tasks += 1;

                }

                var percentage = (mentorship_task_completed / mentorships_all_tasks) * 100;

                console.log(percentage);

                $('.progress-bar').width(percentage + '%');
                $('.progress-bar').html(percentage + '%');



                $('#levup-tasks').html(mentorships_tasks_text);



                console.log(response);



                $('.task-delete-chk').on('click', function (e) {
                    e.preventDefault();

                    console.log('clicked');
                    var parent = $("#chek-" + $(this).attr('data-id'));

                    parent.hide();

                    console.log(parent);


                    var data = {
                        action: 'levup_task_process',
                        task_id: $(this).attr("data-id"),
                        mentorship_id: $("#mentorship_id")[0].value,
                        process: 'task_delete'
                    };
                    document.getElementById("overlay").style.display = "flex";

                    $.post(levup_obj.ajax_url, data,
                        function (response) {

                            update_progress_bar(response.tasks);

                            document.getElementById("overlay").style.display = "none"; console.log(response);
                        }
                    );


                })


                $('.task-done-chk').on('click', function (e) {

                    console.log('clicked');
                    var parent = $("#chek-" + $(this).attr('data-id'));

                    console.log(parent);


                    var data = {
                        action: 'levup_task_process',
                        task_id: $(this).attr("data-id"),
                        mentorship_id: $("#mentorship_id")[0].value,
                        process: 'task_complete',
                        checked: $(this).prop('checked') == true
                    };
                    document.getElementById("overlay").style.display = "flex";

                    $.post(levup_obj.ajax_url, data,
                        function (response) {

                            update_progress_bar(response.tasks);

                            document.getElementById("overlay").style.display = "none";
                            console.log(response);
                        }
                    );


                })


                $('#levup-add-task-form').trigger("reset");

            }
        );


    });
    function on() {
        document.getElementById("overlay").style.display = "flex";
    }

    function off() {
        document.getElementById("overlay").style.display = "none";
    }

    $('#auto-match-button').on('click', function (e) {

        var data_array = $('#levup-filter-mentors').serializeArray();
        var data_array = jQFormSerializeArrToJson(data_array);

        data_array.action = 'levup_auto_match';

        $.post(levup_obj.ajax_url, data_array,
            function (response) {

                if (response.results.length != 0) {
                    var min = 0;
                    var max = response.results.length - 1;

                    var number = Math.round(getRandomArbitrary(min, max));

                    var mentor_id = response.results[number];

                    console.log(levup_obj.site_url + '/request-a-mentorship/?mentor=' + mentor_id);

                    //        document.location.href = levup_obj.site_url+ '/request-a-mentorship/?mentor=' + mentor_id;

                }

            }
        );


        console.log(data_array);
    })


    function update_progress_bar(mentorships_tasks) {
        var mentorship_task_completed = 0;
        var mentorships_all_tasks = 0;

        for (const key in mentorships_tasks) {

            if (mentorships_tasks[key].task_completed == 'YES') {
                mentorship_task_completed += 1;
            }
            mentorships_all_tasks += 1;

        }

        $('#task-completed-count').html(mentorship_task_completed + '/' + mentorships_all_tasks);


        var percentage = Math.round((mentorship_task_completed / mentorships_all_tasks) * 100);


        if (percentage != 100) {
            $('#finish-mentorship').attr('data-toggle', 'tooltip');
            $('#finish-mentorship').attr('disabled', true);
            $('#finish-mentorship').attr('data-placement', 'top');
            $('#finish-mentorship').attr('title', 'Mentorship can\'t be marked as finished until all tasks are completed ');

        }
        else {

            $('#finish-mentorship').attr('disabled', false);
        }




        console.log(percentage);

        $('.progress-bar').width(percentage + '%');
        $('.progress-bar').html(percentage + '%');


    }

    function add_files_and_urls(files_and_urls) {

        var files_and_urls_text = '';

        for (const key in files_and_urls) {


            //  var task_delete = true ? '<a  class="task-delete-chk button" data-id="'+ key + '" style="background:transparent;border:none;" ><i class="fa-regular fa-trash-can" style="color: #bf4040;"></i></a>'  : '';

            files_and_urls_text += '<li><a href="' + files_and_urls[key] + '" >' + key + '</a></li>';

            $('#uploaded-files').html(files_and_urls_text);

        }

    }

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    if (levup_attachments === undefined) {
        var levup_attachments = {};

        levup_attachments.type = '';

        levup_attachments.items = [];
    }

    console.log(levup_attachments);

    $('.message-upload-files').on('change', function () {
        var data = new FormData();

        var type = $(this).attr("data-type");

        var action = '';
        var nonce = '';

        if (type == 'image') {
            action = 'media_upload';
            nonce = BP_Nouveau.nonces.media;
        }
        else if (type == 'video') {
            action = 'video_upload';
            nonce = BP_Nouveau.nonces.video;
        }
        else if (type == 'document') {
            action = 'document_document_upload';
            nonce = BP_Nouveau.nonces.media;
        }



        data.append('file', $(this).prop('files')[0]);
        data.append('action', action);
        data.append('_wpnonce', nonce);
        data.append('from', 'message');
        //       document.getElementById("overlay").style.display = "flex";
        data.append('thread_id', $('#message').attr("data-thread-id"));
        //       data.append('mentee_id', current_mentorship_data[0].mentee_id);


        $.ajax({
            method: 'post',
            processData: false,
            contentType: false,
            cache: false,
            data: data,
            enctype: 'multipart/form-data',
            url: levup_obj.ajax_url,
            success: function (response) {

                if (response.success == true) {
                    if (type == levup_attachments.type) {
                        levup_attachments.items.push(response.data);
                    }
                    else {
                        levup_attachments.items = [];
                        levup_attachments.type = type;
                        levup_attachments.items.push(response.data);

                    }

                    console.log(levup_attachments);


                    //   console.log($(this).prop('files')[0]);

                    var text = '';
                    levup_attachments.items.forEach(element => {

                        console.log(element);
                        text += '<div  class="dz-preview dz-processing dz-image-preview dz-success dz-complete"><div class="dz-image"> <img src="' + element.thumb + '" > </div> <a class="dz-remove levup-attachment-remove" data-id=" ' + element.id + ' " href="javascript:undefined;" data-dz-remove="">Remove file</a></div><div class="dz-global-progress"><div class="dz-progress-bar-full"><span class="dz-progress" style="width: 100%;"></span></div><p>Uploading <strong>WhatsApp Image 2022-02-03 at 02.02.54.jpeg</strong></p><span class="bb-icon-f bb-icon-times dz-remove-all"></span></div></div></div>';

                    });

                    $('#levup-messages-attachments').html(text);
                    $('.messages-media-container').show();


                    register_delete_attachment_handler();

                    //           add_files_and_urls(response.all_files);
                    //         document.getElementById("overlay").style.display = "none";

                    console.log(levup_attachments);
                }


            }
        });

    })

    var getUTCDateTime = function () {
        var current_utc = new Date(),
            day = current_utc.getUTCDate(),
            month = current_utc.getUTCMonth() + 1,
            year = current_utc.getUTCFullYear(),
            hours = current_utc.getUTCHours(),
            minutes = current_utc.getUTCMinutes(),
            seconds = current_utc.getSeconds();

        day = (10 > day) ? '0' + day : day;
        month = (10 > month) ? '0' + month : month;
        hours = (10 > hours) ? '0' + hours : hours;
        minutes = (10 > minutes) ? '0' + minutes : minutes;
        seconds = (10 > seconds) ? '0' + seconds : seconds;

        return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    }

    function register_delete_attachment_handler() {

        $('.levup-attachment-remove').on('click', function () {

            var attachment_id = $(this).attr('data-id').trim();

            var data = {
                'action': 'media_delete_attachment',
                '_wpnonce': BP_Nouveau.nonces.media,
                'id': attachment_id.trim(),
            };

            console.log('clicked');

            $.post(levup_obj.ajax_url, data,
                function (response) {

                    if (response.success == true) {
                        levup_attachments.items = levup_attachments.items.filter((value) => {

                            console.log(value.id, attachment_id);

                            console.log(value.id != attachment_id);

                            return value.id != attachment_id;
                        });

                        console.log(levup_attachments.items);

                        if (levup_attachments.items.length != 0) {
                            var text = '';
                            levup_attachments.items.forEach(element => {

                                console.log(element);
                                text += '<div  class="dz-preview dz-processing dz-image-preview dz-success dz-complete"><div class="dz-image"> <img src="' + element.thumb + '" > </div> <a class="dz-remove levup-attachment-remove" data-id="' + element.id + '" href="javascript:undefined;" data-dz-remove="">Remove file</a></div><div class="dz-global-progress"><div class="dz-progress-bar-full"><span class="dz-progress" style="width: 100%;"></span></div><p>Uploading <strong>WhatsApp Image 2022-02-03 at 02.02.54.jpeg</strong></p><span class="bb-icon-f bb-icon-times dz-remove-all"></span></div></div></div>';

                            });

                            $('#levup-messages-attachments').html(text);
                            $('.messages-media-container').show();
                            register_delete_attachment_handler();

                        }
                        else {
                            $('.messages-media-container').hide();
                        }
                    }

                }
            );

        })
    }

    window.Backbone.listenTo(Backbone, 'onReplySentSuccess', (messagePusherData) => { levup_add_message(messagePusherData) });



    function levup_add_message(messagePusherData) {



        var threads_text = '';

        var messages = [];

        if (levup_obj.current_user_id == messagePusherData.message.sender_id) {
            return;
        }

        messages.push(messagePusherData.message);

        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch


        messages.forEach(message => {
            threads_text += '<li class=""> <div class="bp-single-message-wrap"> <div class="bp-avatar-wrap"> <a href="' + message.sender_link + '" class="bp-user-avatar"> <img class="avatar" src="' + message.sender_avatar + '" alt=""> </a> </div> <div class="bp-single-message-content"> <div class="message-metadata"> <a href="' + message.sender_link + '" class="bp-user-link"> <strong>' + message.sender_name + '</strong> </a> <time datetime="' + d.setUTCSeconds(message.date) + '" class="activity">' + message.display_date + '</time> </div> <div class="bp-message-content-wrap"><p>' + message.content + '</p> </div> ';

            if (message.media != undefined) {
                threads_text += '<div class="bb-activity-media-wrap bb-media-length-1">';
                message.media.forEach(element => {

                    threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="' + element.privacy + '" href="' + element.full + '"> <img  src="' + element.thumbnail + '" alt="' + element.thumbnail + '"> </a> </div> '

                });
                threads_text += '</div>';
            }
            if (message.video != undefined) {
                console.log(message.video_html)

                threads_text += '<div class="bb-activity-video-wrap bb-video-length-1 embed-responsive" >'

                message.video.forEach(element => {

                    //   threads_text += ' <div class="bb-activity-media-elem"> <a class="bb-open-media-theatre bb-photo-cover-wrap bb-item-cover-wrap" data-id="' + element.id + '" data-attachment-id="' + element.attachment_id + '" data-attachment-full="' + element.full + '" data-privacy="'+ element.privacy +'" href="#"> <img  src="' + element.thumbnail + '" alt="'+ element.thumbnail +'"> </a> </div> ';

                    element.video_html = element.video_html.replace('<video', '<video style="width:100%;height:100%; "');

                    threads_text += element.video_html;

                });

                threads_text += '</div>'
            }
            if (message.document != undefined) {
                console.log(message.document)

                threads_text += '<div style="width:90%;" class="bb-activity-media-wrap bb-media-length-1 embed-responsive" >'

                message.document.forEach(element => {
                    console.log(element)

                    threads_text += '<div style="width:90%;"  class="bb-activity-media-elem document-activity " data-id="' + element.id + ' "> <div class="document-description-wrap"> <a href="' + element.url + ' " class="entry-img" data-id="4" data-activity-id="4"> <i class="bb-icon-file-pdf"></i> </a> <a href="' + element.url + ' " class="document-detail-wrap bb-open-document-theatre" data-id="' + element.id + ' " data-activity-id="" data-icon-class="bb-icon-file-pdf&quot;" data-attachment-id="' + element.attachment_id + ' " data-attachment-full="" data-privacy="message" data-extension="' + element.extension + ' " data-author="' + element.author + ' " data-preview="" data-full-preview="" data-text-preview="' + element.text_preview + ' " data-mp3-preview="" data-document-title="' + element.document_title + ' " data-video-preview="" data-mirror-text=""> <span class="document-title">' + element.document_title + ' </span> <span class="document-description">' + element.size + ' </span> <span class="document-extension-description">' + element.extension + ' </span> <span class="document-helper-text"> <span> - </span> <span class="document-helper-text-inner">Click to view</span></span> </a> </div> <div class="document-action-wrap"> <a href="#" class="document-action_collapse" data-balloon-pos="up" data-tooltip-collapse="Collapse" data-balloon="Expand"><i class="bb-icon-merge bb-icon-l document-icon-collapse"></i></a> <a href="' + element.url + ' " class="document-action_download" data-balloon-pos="up" data-balloon="Download"> <i class="bb-icon-l bb-icon-download"></i> </a> <a href="#" target="_blank" class="document-action_more" data-balloon-pos="up" data-balloon="More actions"> <i class="bb-icon-f bb-icon-ellipsis-h"></i> </a> <div class="document-action_list"> <ul> <li class="copy_download_file_url"> <a href="' + element.url + ' ">Copy Download Link</a> </li> </ul> </div> </div> </div>';

                });

                threads_text += '</div>'
            }

            threads_text += '</div> </div> </li>';

        });

        var orignal_text = $('#levup-message-thread-list').html();


        var updated_text = orignal_text + threads_text;

        $('#levup-message-thread-list').html(updated_text);
    }

    var anchors = document.getElementsByTagName("a");

    console.log(levup_obj.current_user);


    console.log(window.location.href);


    if (window.location.href.includes(levup_obj.site_url + '/members/' + levup_obj.current_user.data.user_nicename + '/messages/')) {
        window.location.href = levup_obj.site_url + '/chatbox/';
    }

    for (var i = 0; i < anchors.length; i++) {

        //    console.log(levup_obj.site_url + 'members' + levup_obj.current_user.data.user_nicename + '/messages/');

        if (anchors[i].href.includes(levup_obj.site_url + '/members/' + levup_obj.current_user.data.user_nicename + '/messages/')) {
            anchors[i].href = levup_obj.site_url + '/chatbox/';
            console.log(anchors[i].href);
        }

    }


    window.Backbone.listenTo(Backbone, 'onSentMessage', (messagePusherData) => { console.log(messagePusherData); });
    window.Backbone.listenTo(Backbone, 'onSentMessageError', (messagePusherData) => { console.log(messagePusherData); });
    window.Backbone.listenTo(Backbone, 'onReplySentSuccess', (messagePusherData) => { console.log(messagePusherData); });
    window.Backbone.listenTo(Backbone, 'onReplyReSend', (messagePusherData) => { console.log(messagePusherData); });
    window.Backbone.listenTo(Backbone, 'onMessageDeleteSuccess', (messagePusherData) => { console.log(messagePusherData); });
    window.Backbone.listenTo(Backbone, 'onMessageAjaxFail', (messagePusherData) => { console.log(messagePusherData); });
})(jQuery);