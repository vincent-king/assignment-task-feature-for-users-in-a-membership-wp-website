<?php
/**
 * Find a Mentor.
 *
 * @return void
 */
function request_a_mentorship() {

	
	if ( ! is_user_logged_in() ) {

		return 'You need to be logged in to access this page';

	}



	if ( ! isset( $_GET['mentor'] ) || empty( $_GET['mentor'] ) ) {

		$content = 'Invalid Mentor';

		return $content;

	}

	$mentor_id       = $_GET['mentor'];
	$current_user_id = get_current_user_id();

	// var_dump( check_mentorship_availability( $mentor_id, $current_user_id ) );

	if ( $mentor_id == $current_user_id || ! check_mentorship_availability( $mentor_id, $current_user_id ) || ! check_mentorship_availability( $mentor_id ) || ! check_mentorship_availability( $current_user_id ) ) {

		$content = 'Mentor Already Booked or Mentor is Unavailable';

		return $content;

	}

	$user_data = get_xprofile_data( $mentor_id );

	$user_mentorship_level = $user_data['groups']['Details']['fields']['Profile Type']['raw'];

	// //var_dump( $user_mentorship_level != 'Mentor' || $user_mentorship_level != 'Master' );

	if ( $user_mentorship_level != 'Mentor' && $user_mentorship_level != 'Master' ) {
		$content = 'User not Available for Mentorship';

		return $content;
	}

	global $wpdb;

	$image_folder = plugins_url( 'assets', LEVUP_PATH );

	global $wp_styles;
	//$wp_styles->queue = array();

	require LEVUP_DIR_PATH . 'components/request-a-mentorship/main-content.php';

	$content = "<div id='levup_components' >" .
	levup_header() . levup_user_welcome() . request_a_mentorship_content( $mentor_id )
	. '	</div>';
	$content = str_replace( 'assert', $image_folder, $content );

	/*
	dboard_header();
	dboard_user_welcome();
	dboard_stats_all_the_time();
	dboard_3_steps();
	dboard_current_mentorships_and_history();
	*/

	return $content;

}
