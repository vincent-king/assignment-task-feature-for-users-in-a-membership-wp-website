<?php
/**
 * Find a Mentor.
 *
 * @return void
 */
function levup_chat_box() {

	
	if ( ! is_user_logged_in() ) {

		return 'You need to be logged in to access this page';

	}


	global $wp_styles;
	// $wp_styles->queue = array();

	require LEVUP_DIR_PATH . 'components/chatbox/main-content.php';

	$content = "<div id='levup_components' >" .
	levup_header() . chatbox_main_content()
	. '	</div>';

	/*
	dboard_header();
	dboard_user_welcome();
	dboard_stats_all_the_time();
	dboard_3_steps();
	dboard_current_mentorships_and_history();
	*/
	$image_folder = plugins_url( 'assets', LEVUP_PATH );

	$content = str_replace( '/images', $image_folder . '/images', $content );

	return $content;

}
