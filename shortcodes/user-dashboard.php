<?php

/**
 * Displays the user dashboard;
 *
 * @return void
 */
function display_user_dashboard() {

	
	if ( ! is_user_logged_in() ) {

		return 'You need to be logged in to access this page';

	}



	require LEVUP_DIR_PATH . 'components/dashboard/style.php';
	require LEVUP_DIR_PATH . 'components/dashboard/stats-all-time.php';
	require LEVUP_DIR_PATH . 'components/dashboard/3-steps.php';
	require LEVUP_DIR_PATH . 'components/dashboard/current-mentorships-and-history.php';

	$content = "<div id='levup_components' >" . get_dashboard_style() .
	levup_header() . levup_user_welcome() . dboard_stats_all_the_time() . dboard_3_steps() . dboard_current_mentorships_and_history()
	. '	</div>';

	/*
	dboard_header();
	dboard_user_welcome();
	dboard_stats_all_the_time();
	dboard_3_steps();
	dboard_current_mentorships_and_history();
	*/

	$image_folder = plugins_url( 'assets', LEVUP_PATH );

	global $wp_styles;
	//$wp_styles->queue = array();

	$content = str_replace( 'assets', $image_folder, $content );
	$content = str_replace( 'assert', $image_folder, $content );

	return $content;

}
