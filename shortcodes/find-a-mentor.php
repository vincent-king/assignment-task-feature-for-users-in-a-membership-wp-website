<?php
/**
 * Find a Mentor.
 *
 * @return void
 */
function find_a_mentor() {

	if ( !is_user_logged_in() ) {
	
		return 'You need to be logged in to access this page';
		
	}

	global $wp_styles;
	//$wp_styles->queue = array();

	require LEVUP_DIR_PATH . 'components/find-a-mentor/main-content.php';

	wp_register_style( 'levup2_stye2_css', plugins_url( 'assets/css/style2.css', LEVUP_PATH ), array(), time() );
	wp_register_style( 'levup2_media2_css', plugins_url( 'assets/css/media2.css', LEVUP_PATH ), array(), time() );
	wp_enqueue_style( 'levup2_stye2_css' );
	wp_enqueue_style( 'levup2_media2_css' );

	$content = "<div id='levup_components' >" .
	levup_header() . levup_user_welcome() . find_a_mentor_content()
	. '	</div>';

	/*
	dboard_header();
	dboard_user_welcome();
	dboard_stats_all_the_time();
	dboard_3_steps();
	dboard_current_mentorships_and_history();
	*/

	return $content;

}
