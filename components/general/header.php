<?php

function levup_header() {
	$content = '
		
	<!-- Header -->
	<header>
		<div class="container">
			<div class="row">
				<ul>
					<li><a class="active" href="' . get_site_url() . '/user-dashboard/">DASHBOARD</a></li>
					<li><a href="' . get_site_url() . '/chatbox">GOALS IN PROGRESS</a></li>
					<li><a href="#">RULES</a></li>
				</ul>
			</div>
		</div>
	</header>
	';

	return $content;

}
