<?php

function request_a_mentorship_content( $mentor_id ) {

		$mentor = get_user_by( 'id', $mentor_id );

		$mentor_data = get_xprofile_data( $mentor_id );

		$name                  = $mentor_data['groups']['Details']['fields']['First Name']['raw'] == null ? 'No Name' : $mentor_data['groups']['Details']['fields']['First Name']['raw'];
		$profession            = $mentor_data['groups']['Details']['fields']['Profession']['rendered'] == null ? 'Profession Unavailable' : $mentor_data['groups']['Details']['fields']['Profession']['rendered'];
		$experience_level      = $mentor_data['groups']['Mentor Ship']['fields']['Experience Level']['rendered'] == null ? 'Experience Level Unavailable' : $mentor_data['groups']['Mentor Ship']['fields']['Experience Level']['rendered'];
		$more_details          = $mentor_data['groups']['Professional']['fields']['More Details']['rendered'] == null ? 'Details to be included' : $mentor_data['groups']['Professional']['fields']['More Details']['rendered'];
		$user_mentorship_level = $mentor_data['groups']['Details']['fields']['Profile Type'];

		$mentorship_offered = '';

	foreach ( $mentor_data['groups']['Professional']['fields']['Mentorship Type']['unserialized'] as $offered ) {

		$mentorship_offered         .= '<li>' . $offered . '</li>';
		$type_of_mentorship_options .= '     <label for="cb1" class="ip1"><input value="' . $offered . '" type="checkbox" name="mentorships_selected[]" id="cb1" >' . $offered . '</label>';

	}
	$date = date( 'Y-m-d' );
	// var_dump($date);

		$args = '';

		$defaults = array(
			'type'   => 'thumb',
			'width'  => 100,
			'height' => 100,
			'class'  => 'avatar',
			'id'     => false,
			'alt'    => sprintf( __( 'Profile photo of %s', 'buddyboss' ), $fullname ),
		);
		$r        = wp_parse_args( $args, $defaults );
		extract( $r, EXTR_SKIP );

		$content = '<section class="requestSectionTwo" id="main-element-section">
        <div class="container">
            <div class="container3">
                <h3 style="color: black">Selected Mentor</h3>
            </div>
            <div class="container4">
                <div class="part1">
                    <div class="part11" style="flex-direction: column !important;">
                        <div class="part111">
                        <div class="fst">
                        <div style="position: relative">
                            ' . bp_core_fetch_avatar(
			array(
				'item_id' => $mentor->ID,
				'type'    => $type,
				'alt'     => $alt,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => $mentor->data->user_mail,
			)
		) . '    <img src="assert/images/AAAAA%201.png"  style="position: absolute; transform: scale(0.5);left: 20px;top:15px" />
                          
                        </div>

                        <div>
                            <ul>
                          
                                <li class="alina"><b>  <a href="' . get_site_url() . '/members/' . $mentor->data->user_nicename . '" > ' . $name . '</a></b></li>
                                <li class="ce">Computer Engineer</li>
                            </ul>
                        </div>
                    </div>  <div class="bronze">' . $experience_level . '</div>

                        </div>
                        <div class="para">
                        ' . $more_details . '
                        </div>
                        <div class="offer">
                            <b> Offer</b>
                        </div>
                        <div class="categories">
                            <ul>
                            ' . $mentorship_offered . '
                            </ul>
                        </div>

                    </div>
                    <div class="part12">
                    
                        <div class="updown"><a href="' . get_site_url() . '/find-a-mentor/' . '" > Select different Mentor</a></div>
                    </div>


                </div>
                <div class="part2">
                    <div class="req">
                        Send a Request to ' . $name . '
                    </div>
                    <form id="request-a-mentorship-form" >                    
                    <input type="hidden" name="mentor_id" value="' . $mentor_id . ' " >
                    <input type="hidden" name="action" value="levup_request_mentorship" >

                    ' . wp_nonce_field( 'levup_request_mentorship_nonce' ) . '
                    <div class="all">
                        <p>Type of mentorship (Select all that applies)</p>
                    </div>
                         <div class="inputs">

                       ' . $type_of_mentorship_options . '
                        </div>
                    <div class="timeline">
                        Prefer Timeline
                    </div>
   
                   <input required type="date" name="timeline" style="min-width:100%;" min="' . $date . '" >
                    <div class="timeline">
                        Intro Message

                    </div>
 
                        <textarea required name="message" id="message" cols="30" rows="10"
                                  placeholder="Type message Here"></textarea>
                        <input type="submit" value="SEND A REQUEST" id="submit">
 </form>
                </div>

            </div>
        </div>
    </section>
    ';

		return $content;
}
