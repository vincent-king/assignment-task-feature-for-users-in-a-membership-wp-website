<?php

function find_a_mentor_content() {

	$image_folder = plugins_url( 'assets', LEVUP_PATH );

	global $wpdb;

	$filters = array();

	$request_mentorship_page = home_url( 'request-a-mentorship' );

	$industry_id = $wpdb->get_var( "SELECT ID FROM {$wpdb->prefix}bp_xprofile_fields where name='industry'" );

	$industry_info = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}bp_xprofile_fields where parent_id='{$industry_id}'" );

	$mentorship_type_id = $wpdb->get_var( "SELECT ID FROM {$wpdb->prefix}bp_xprofile_fields where name='Mentorship Type'" );

	$mentorship_type_info = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}bp_xprofile_fields where parent_id='{$mentorship_type_id}'" );

	$filters['Industry']        = $industry_info;
	$filters['Mentorship Type'] = $mentorship_type_info;
	$industry_text              = '';
	foreach ( $filters['Industry'] as $key => $value ) {
		$industry_text .= '                    <div class="form-check">
        <input class="form-check-input" name="industry[]" type="checkbox" value="' . $value->name . '" id="flexCheckDefault" >
        <label class="form-check-label text-dark" for="flexCheckDefault">
        ' . $value->name . '
        </label>
    </div>';
	}

	$mentorship_type_text = '';
	foreach ( $filters['Mentorship Type'] as $key => $value ) {
		$mentorship_type_text .= '                        <div class="form-check">
        <input class="form-check-input" type="checkbox" name=mentorship_type[] value="' . $value->name . '" id="flexCheckDefault" >
        <label class="form-check-label text-dark" for="flexCheckDefault">
        ' . $value->name . '
        </label>
    </div>';
	}

	$users = get_users();

	$users_content_html = '';

	foreach ( $users as $user ) {

		$args = array();

		$defaults = array(
			'type'   => 'thumb',
			'width'  => false,
			'height' => false,
			'class'  => 'avatar',
			'id'     => false,
			'alt'    => sprintf( __( 'Profile photo of %s', 'buddyboss' ), $user->data->display_name ),
		);
		$r        = wp_parse_args( $args, $defaults );
		extract( $r, EXTR_SKIP );

		$groups = get_xprofile_data( $user->ID );

		$mentorship_offered = '';

		$name                         = $groups['groups']['Details']['fields']['First Name']['rendered'] == null ? 'No Name' : $groups['groups']['Details']['fields']['First Name']['rendered'];
		$profession                   = $groups['groups']['Details']['fields']['Profession']['rendered'] == null ? 'Profession Unavailable' : $groups['groups']['Details']['fields']['Profession']['rendered'];
		$experience_level             = $groups['groups']['Mentor Ship']['fields']['Experience Level']['rendered'] == null ? 'Experience Level Unavailable' : $groups['groups']['Mentor Ship']['fields']['Experience Level']['rendered'];
		$more_details                 = $groups['groups']['Professional']['fields']['More Details']['rendered'] == null ? 'Details to be included' : $groups['groups']['Professional']['fields']['More Details']['rendered'];
		$user_mentorship_level        = $groups['groups']['Details']['fields']['Profile Type']['raw'];
		$user_mentorship_offered_type = $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'] == null ? array() : $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'];

		$user_availability = check_mentorship_availability( $user->ID ) ? 'style="display:none"' : '';

		// var_dump( $user_mentorship_level != 'Mentor' || $user_mentorship_level != 'Master' );
		if ( $user->ID == get_current_user_id() ) {
			continue;
		}

		if ( $user_mentorship_level != 'Mentor' && $user_mentorship_level != 'Master' ) {
			continue;
		}

		if ( empty( $user_mentorship_offered_type ) ) {
			continue;
		}

		$booked            = check_mentorship_availability( $user->ID, get_current_user_id() ) ? 'style="display:none"' : '';
		$hide_start_button = check_mentorship_availability( $user->ID, get_current_user_id() ) ? '' : 'style="display:none"';
		// var_dump($user_mentorship_offered_type);

		foreach ( $groups['groups']['Professional']['fields']['Mentorship Type']['unserialized'] as $offered ) {

			$mentorship_offered .= '<i class="bi bi-check-lg"></i><span> ' . $offered . '</span><br>';

		}
		$users_content_html .= '                        <div class="col-sm-6 mt-4">
        <div class="card1">
            <div class="cardbx">
                <div class="img12">
                    ' . bp_core_fetch_avatar(
			array(
				'item_id' => $user->ID,
				'type'    => $type,
				'alt'     => $alt,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => $user->data->user_email,
			)
		) . '
                </div>
                <div class="box7">
        <h5><a href="' . get_site_url() . '/members/' . $user->data->user_nicename . '" > ' . $name . '</a></h5>
                    <h6>' . $profession . '</h6>
                </div>
                <a href="">' . $experience_level . '</a>
            </div>
            <p>' . $more_details . '</p>
            <h2>Offer</h2>
            ' . $mentorship_offered . '
            <div class="btn19 text">
            <a class="btn" ' . $hide_start_button . ' href="' . $request_mentorship_page . '?mentor=' . $user->ID . '"> Start with Mentorship </a>
                <button style="display:none;" href="' . $request_mentorship_page . '?mentor=' . $user->ID . '" class="btn">Start with Mentorship</button>
            </div>
            <h6 ' . $booked . ' class="text-dark text-center mt-4 mb-2">User Booked</h6>
            <h6 ' . $user_availability . ' class="text-dark text-center mt-4 mb-2">Fully Booked</h6>
        </div>
    </div>';

	};

	// echo '<pre>';

	// //var_dump( get_xprofile_data( 1 ) );

	// echo '</pre>';

	$content = '
		<section class="sectionsix">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 none left-portion">
                    <h2>Find A Mentor</h2>
                    <div class="check">
                    <form id="levup-filter-mentors" >
                    <input type="hidden" name="action" value="levup_filter_mentors" >

                    ' . wp_nonce_field( 'levup_filter_mentors_nonce' ) . '
                        <h6>Name</h6>
                        <input type="search" name="search" class="form-control" aria-label="Search" aria-describedby="search-addon" />
                        <h6>Mentor Types</h6>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="mentor_level[]" value="mentor" id="flexCheckDefault" >
                            <label class="form-check-label text-dark" for="flexCheckDefault">
                                Mentors
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"  name="mentor_level[]" type="checkbox" value="master" id="flexCheckDefault" >
                            <label class="form-check-label m-1 text-dark" for="flexCheckDefault">
                                Masters
                            </label>
                            <span>Only silver+ rank</span>
                        </div>
                        <h6>Industry</h6>
         ' . $industry_text . '

                        <h6>Mentorship Type</h6>
' . $mentorship_type_text . '
                        <div class="button1 m-4">
                            <button type="submit" class="btn">Apply</button>
                        </div>
                        </form>
                    </div>
                    <div class="box9">
                        <div class="card0 text-center">
                            <img src="assert/images/download (2) 1.png" alt="">
                            <h3>Only available for <br>
                                Silver members</h3>
                                <p>We/\'ll show you more stories from the topics you pick</p>
                                <a href="">How to become Bronze +</a><br><br>
                                
                            </div>
                    </div>
                </div>
                <div class="col-sm-8 right-portion">
                    <div class="textbox1">
                        <div class="flex">
                            <div class="img3">
                                <img src="assert/images/16.png" alt="" >
                            </div>
                            <div class="box5">
                                <span>New</span>
                                <h2>Auto Match with Mentor</h2>
                                <p>You’ll be match with an expert in a second.</p>
                            </div>
                        </div>
                        <div class="btnbox">
                            <button class="btn" id="auto-match-button" >
                                <img src="assert/images/Group 1.png" alt="" >
                                AUTO MATCH
                            </button>
                        </div>
                    </div>
                    <div class="drop" style="color: black; padding: 0 10px">
                        <h4>Find A Mentor</h4>
                        <p href="">Filters</p>
                    </div>
                    <div class="row" id="mentors-results" >
                    ' . $users_content_html . '
                    </div>
                </div>
            </div>
        </div>
    </section><br><br>';

	$data = bp_xprofile_get_groups();

	$content = str_replace( 'assert', $image_folder, $content );

	return $content;

}

