<?php

function get_dashboard_style() {
	$content = '<style>
    .sectiontwo {
        z-index: 2;
    }

    .sectiontwo .s2container {
        background-color: #B7B7B7;
        border-bottom-left-radius: 12px;
        border-bottom-right-radius: 12px;
        height: 10%;
        padding: 14px 40px 40px 40px;
        color: #4F4429;
    }

    .sectiontwo .s2container .icons-board {
        display: flex;
        justify-content: space-between;
    }

    .sectiontwo .s2container .icons-board .icon-box {
        display: flex;
        align-items: center;
    }

    .sectiontwo .s2container .icons-board .icon-box .img-box {
        background-color: white;
        height: 50px;
        width: 50px;
        border-radius: 100%
    }

    .sectiontwo .s2container .icons-board .icon-box .img-box .img1 {
        /*transform: scale(0.9);*/
        margin: -10px 0 0 10px
    }

    .sectiontwo .s2container .icons-board .icon-box .img-box .img2 {
        /*transform: scale(0.5);*/
        margin: -10px 0 0 14px
    }

    .sectiontwo .s2container .icons-board .icon-box .img-box .img3 {
        /*transform: scale(0.9);*/
        margin: -10px 0 0 12px
    }

    .sectiontwo .s2container .icons-board .icon-box .img-box .img4 {
        /*transform: scale(0.8);*/
        margin: -10px 0 0 10px
    }

    .sectiontwo .s2container .icons-board .icon-box .text1 {
        margin-left: 10px;
        color: white;
    }

    .sectiontwo .s2container .icons-board .icon-box .text1 h5 {
        font-size: 30px;
        font-weight: 400;
    }

    .sectiontwo .s2container .icons-board .icon-box .text1 h6 {
        font-size: 12px;
        font-weight: 400;
    }

</style>
';

	return $content;

}
