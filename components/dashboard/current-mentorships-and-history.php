<?php

function dboard_current_mentorships_and_history() {

	$current_user_id = get_current_user_id();

	$current_mentorships = get_current_mentorships();

	$current_mentorships_text = '';

	$current_mentorships_count = check_mentorship_availability( $current_user_id, false, true );

	$current_mentorships_left = 4 - intval( $current_mentorships_count );

	$mentorship_history = get_mentorship_history();

	$past_mentors = count( $mentorship_history['mentors'] );

	$past_mentees = count( $mentorship_history['mentee'] );

	$current_user_threads = get_user_threads( $current_user_id );

	$current_user = get_user_by( 'ID', $current_user_id );

	// var_dump($thread_ids);

	// var_dump( 5, intval($current_mentorships_count));

	$args     = array();
	$fullname = $current_user->data->display_name;

	$defaults = array(
		'type'   => 'thumb',
		'width'  => 65,
		'height' => 65,
		'class'  => 'avatar',
		'id'     => false,
		'alt'    => sprintf( __( 'Profile photo of %s', 'buddyboss' ), $fullname ),
	);
	$r        = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	// var_dump($current_mentorships);

	foreach ( $current_mentorships as $mentorship ) {
		if ( $mentorship['mentor_id'] != $current_user_id ) {
			$partner_id = $mentorship['mentor_id'];
		} elseif ( $mentorship['mentee_id'] != $current_user_id ) {

			$partner_id = $mentorship['mentee_id'];
		}

		$partner_threads = get_user_threads( $partner_id );

		$unique_thread = array_intersect( $partner_threads, $current_user_threads );

		$partner = get_user_by( 'ID', $partner_id );

		$tasks = maybe_unserialize( $mentorship['tasks'] );

		$mentorship_task_completed = 0;
		$mentorships_all_tasks     = 0;

		foreach ( $tasks as $key => $value ) {

			if ( $tasks[ $key ]['task_completed'] == 'YES' ) {
				$mentorship_task_completed += 1;
			}
			$mentorships_all_tasks += 1;

		}

		if ( $mentorships_all_tasks != 0 ) {
			$percentage = ( $mentorship_task_completed / $mentorships_all_tasks ) * 100;
		} else {
			$percentage = 0;
		}

		// var_dump($partner);
		/*
		$current_mentorships_text .= '<div class="card2 d-flex ">
		<div class="imgs img-active d-flex align-items-center">
		<img src="assert/images/ellipse-131@2x.png" height="65px" alt="">
		</div>
		<div class="middle-flex">
		<div class="text2">
			<h5>Sergey</h5>
			<span><u>Start a Meeting</u> task is completed</span>
		</div>
		<div class="progresss d-flex align-items-center" style="width: fit-content">
			<progress value="65" max="100" style="border-radius: 50%;"> 65%</progress>
		</div>
		</div>
		<div class="arrow-icon"><i class="bi bi-chevron-right"> </i></div>

		</div>';
		*/

	
		$pending = $mentorship['mentorship_started'] == 1 ? '' : 'display:none;';

		$current_mentorships_text .= '					<div class="card2 d-flex">


		<div class="imgs img2 d-flex justify-content-between align-items-center">
		<span style="height:3px;position:absolute;' . $pending . ' "  class="badge badge-warning">Pending</span>
		
		' . bp_core_fetch_avatar(
			array(
				'item_id' => $partner_id,
				'type'    => $type,
				'alt'     => $alt,
				'css_id'  => $id,
				'class'   => $class,
				'width'   => $width,
				'height'  => $height,
				'email'   => $partner->data->user_email,
			)
		) . '
	
		</div>
		<div class="middle-flex">
			<div class="text2 text22">
				<h5>' . $partner->data->display_name . '</h5>
				<span><u>Start a Meeting</u> task is completed</span>
			</div>
			
			<div class="progresss d-flex align-items-center" style="width: fit-content">
				<progress class="special-progess" value="' . $percentage . '" max="100" style="border-radius: 50%;"> ' . $percentage . '%
				</progress>
			</div>

		</div>
                        <div  class="arrow-icon"><a href="' . get_site_url() . '/chatbox/?thread=' . $unique_thread[0] . '" > <i class="bi bi-chevron-right"> </i></a></div>
	</div>

	<div><img src="assets/images/Line 3.png" alt="" width="100%"></div>
';

	}
	$past_mentors_text = '';

	if ( empty( $mentorship_history['mentors'] ) ) {
		$past_mentors_text = '<div class="imgbox2">There are no Mentors to show</div>';
	} else {
		foreach ( $mentorship_history['mentors'] as $mentor_id ) {
			$mentor             = get_user_by( 'ID', $mentor_id['mentor_id'] );
			$past_mentors_text .= '								<div class="imgbox2">
			' . bp_core_fetch_avatar(
				array(
					'item_id' => $mentor_id['mentor_id'],
					'type'    => $type,
					'alt'     => $alt,
					'css_id'  => $id,
					'class'   => $class,
					'width'   => $width,
					'height'  => $height,
					'email'   => $mentor->data->user_email,
				)
			) . '
			<span>' . $mentor->data->display_name . '</span>
		</div>';
		}
	}

	$past_mentee_text = '';

	if ( empty( $mentorship_history['mentee'] ) ) {
		$past_mentee_text = '<div class="imgbox2">There are no Mentee to show</div>';
	} else {

		foreach ( $mentorship_history['mentee'] as $mentee_id ) {
			$mentee = get_user_by( 'ID', $mentee_id['mentee_id'] );

			$past_mentee_text .= '								<div class="imgbox2">
			' . bp_core_fetch_avatar(
				array(
					'item_id' => $mentee_id['mentee_id'],
					'type'    => $type,
					'alt'     => $alt,
					'css_id'  => $id,
					'class'   => $class,
					'width'   => 50,
					'height'  => 50,
					'email'   => $mentee->data->user_email,
				)
			) . '
			<span>' . $mentee->data->display_name . '</span>
		</div>';
		}
	}

	 $content = '<section class="sectionfive">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="card" style="margin-left: -10px;">
					<div class="card1 d-flex justify-content-between">
						<h4>Mentorships <span>' . $current_mentorships_count . '/4</span></h4>
						<h3>WORKROOMS <i class="bi bi-arrow-right"></i></h3>
					</div>

					<div><img src="assets/images/Line 3.png" alt="" width="100%"></div>

' . $current_mentorships_text . '
					<div class="card3">
						<div class="btn1  d-flex justify-content-between" style="align-items: center;">
							<div style="display: flex;align-items: center; justify-content: end">
								<svg  style="margin-left:10px;margin-right:5px" width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M18.5 0L21.0578 3.17195L24.5069 1.00238L25.8962 4.83298L29.8629 3.9009L29.9331 7.97504L33.9876 8.38146L32.7311 12.2577L36.4339 13.9585L33.9869 17.2167L36.9368 20.0277L33.5645 22.3148L35.4418 25.9314L31.5096 26.9996L32.1109 31.0297L28.0449 30.7632L27.305 34.7703L23.5458 33.198L21.545 36.7477L18.5 34.04L15.455 36.7477L13.4542 33.198L9.69497 34.7703L8.95513 30.7632L4.88911 31.0297L5.49043 26.9996L1.55819 25.9314L3.43552 22.3148L0.0631866 20.0277L3.01308 17.2167L0.566095 13.9585L4.26888 12.2577L3.01242 8.38146L7.06685 7.97504L7.13706 3.9009L11.1038 4.83298L12.4931 1.00238L15.9422 3.17195L18.5 0Z" fill="#448FFF"/>
									<path d="M24.7408 24.4616L22.0622 21.7385C23.061 20.6 23.6815 19.0923 23.6815 17.4462C23.6815 13.8923 20.8365 11 17.3407 11C13.845 11 11 13.8923 11 17.4462C11 21 13.845 23.8923 17.3407 23.8923C18.5817 23.8923 19.7318 23.5231 20.7003 22.9077L23.4848 25.7385C23.6512 25.9077 23.8782 26 24.1052 26C24.3322 26 24.5593 25.9077 24.7257 25.7385C25.0889 25.3847 25.0889 24.8153 24.7408 24.4616L24.7408 24.4616ZM12.7705 17.4462C12.7705 14.8923 14.8136 12.8 17.3407 12.8C19.8679 12.8 21.8958 14.8923 21.8958 17.4462C21.8958 20.0001 19.8528 22.0923 17.3407 22.0923C14.8287 22.0923 12.7705 20.0154 12.7705 17.4462Z" fill="white"/>
								</svg>
								<span><a href="' . get_site_url() . '/find-a-mentor/' . '" > ADD A MENTOR</a></span>
							</div>
							<h5 style="font-size: 18px;text-align: end;">You have ' . $current_mentorships_left . ' spot
								left</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card" style="margin-right: -10px;">
					<div class="r-card">
						<div class="r-card-left">
							<div class="head2">
								<h2>Past Mentors <span>' . $past_mentors . '</span></h2>
							</div>

							<div class="imgbox1">
							' . $past_mentors_text . '
			
							</div>
						</div>
						<div class="r-card-right">
							<div class="head2">
								<h2>Past Mentees <span>' . $past_mentees . '</span></h2>
							</div>
							<div class="imgbox1" style="overflow: auto; height: 456px;">
							' . $past_mentee_text . '

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
';

	return $content;

}
